#include "hcgamgamanalysis/HcgamAnalysis.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"


// EDM include(s):
#include "EventLoop/Worker.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

// Local include(s):
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/TruthUtils.h"




// this is needed to distribute the algorithm to the workers
ClassImp(HcgamAnalysis)


HcgamAnalysis::HcgamAnalysis(const char *name)
: HgammaAnalysis(name)
, m_goodFakeComb(false)
, m_N_xAOD(0)
, m_N_DxAOD(0)
, m_sumw_xAOD(0.0)
, m_sumw2_xAOD(0.0)
, m_sumw_DxAOD(0.0)
, m_sumw2_DxAOD(0.0)
, m_hcgamgamTool(nullptr){ }


HcgamAnalysis::~HcgamAnalysis() {}

//This function will help to store the variables of interest,
//also will ignore those which are not needed for the MC or Data (see header)
void HcgamAnalysis::declareOutputVariables(TString outName, TString configKey, HG::StrV extra, HG::StrV ignore)
{
  if (config()->isDefined(configKey)) {
    TString vars = config()->getStr(configKey).Data();

    if (m_saveDetailed) {
      TString detailKey = configKey.ReplaceAll("Variables", "DetailedVariables");

      if (config()->isDefined(detailKey)) {
        TString detailed = config()->getStr(detailKey);
        vars += "." + detailed;
      }
    }

    for (TString val : extra)
    { vars += val; }

    for (TString val : ignore)
    { vars = vars.ReplaceAll(val, ""); }

    event()->setAuxItemList((outName + "Aux.").Data(), vars.Data());
  } else { HG::fatal("Cannot find " + configKey); }
}



EL::StatusCode HcgamAnalysis::initialize()
{
  // Make sure that all of our tools here are initialized before calling HgammaAnalysis::initialize()
  // in order to correctly fillSystematicsList()
  HgammaAnalysis::initialize();


  m_hcgamgamTool = new HG::HcgamgamTool("HcgamgamTool",eventHandler(),truthHandler());
  ANA_CHECK(m_hcgamgamTool->initialize(*config()));



  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HcgamAnalysis::createOutput()
{
    //In this section we will create the Containers which will be written into the tree.
    //Those are jets, photons, and event container
    //So far there are NO truth objects
    ///////////////////----------------------------------------------------------
  	m_photonContainerName = "HGam" + config()->getStr("PhotonHandler.ContainerName");
  	m_jetContainerName    = "HGam" + config()->getStr("JetHandler.ContainerName");
  	m_elecContainerName   = "HGam" + config()->getStr("ElectronHandler.ContainerName");
    m_muonContainerName   = "HGam" + config()->getStr("MuonHandler.ContainerName");
    m_truthEvtsName       = "TruthEvents";
    m_evtInfoName         = "EventInfo";
    ///////////////////----------------------------------------------------------

    // What selection to require. For defintion, see CutEnum in the header file
    // Negative values means no event selection (for the variables ON/OFF see the analysis config)
    //=========================Apply skimming===================//
    m_skimCut = config()->getInt("SkimmingCut", -1);

	//++++++++++++++++++++++++++++Is it skimming and slimming job?+++++++++++++++//
    m_skimandslim  = config()->getBool("DoSkimAndSlim", false);

    //=========================Lepton veto cuts===================//
    m_minMuonPT = config()->getInt("minMuonPT", -1);
    m_minElectronPT = config()->getInt("minElectronPT", -1);

    //=========================Allow more than 2 photons===================//
    m_allowMoreThanTwoPhotons = config()->getBool("AllowMoreThanTwoPhotons", false);

    //=========================Enable fake photons===================//
    m_enableFakePhotons = HG::isMC() && config()->getBool("SaveFakePhotonCombinations", false);


    //+++++++++++++++++++++++++++++++Saving: Objects+++++++++++++++++++++++//
    m_saveObjects = config()->getBool("SaveObjects", false);

    //+++++++++++++++++++++++++++++++Saving: Detailed+++++++++++++++++++++++//
    m_saveDetailed = config()->getBool("SaveDetailedVariables", false);

    //+++++++++++++++++++++++++++++++Saving: Truth Objects+++++++++++++++++++++++//
    m_saveTruthObjects = HG::isMC() && config()->getBool("SaveTruthObjects", false);
    m_saveTruthVars    = HG::isMC() && config()->getBool("SaveTruthVariables", false);



    //+++++++++++++++++++++++++++++++Saving: Containers+++++++++++++++++++++++//
    //1. Save Event variables-----------------------
    StrV ignore = {};
    if (HG::isData()) ignore = {".mcChannelNumber", ".mcEventWeights", ".RandomRunNumber", ".truthCentralEventShapeDensity", ".truthForwardEventShapeDensity"};

    StrV trigs = config()->getStrV("EventHandler.RequiredTriggers");
    StrV extra = {};
    for (auto trig: trigs) extra.push_back(".passTrig_"+trig);
    //Save the triggers
    declareOutputVariables(m_evtInfoName,"MxAOD.Variables.EventInfo", extra, ignore);

    //2. Save TruthEvents variables-----------------
  	ignore = {};
  	extra = {};
  	declareOutputVariables(m_truthEvtsName, "MxAOD.Variables.TruthEvents", extra, ignore);


    //3. Save objects-----------------------
    if (HG::isData()) ignore = {".isEMTight_nofudge", ".isTight_nofudge", ".topoetcone20_DDcorrected", ".topoetcone40_DDcorrected", ".truthOrigin", ".truthType", ".truthConvRadius", ".scaleFactor", ".truthLink", ".parentPdgId", ".pdgId","recoj_matchB","recoj_matchC","recoj_matchL"};

    declareOutputVariables(m_photonContainerName, "MxAOD.Variables.Photon", {}, ignore);
    declareOutputVariables("HGamPhotonsWithFakes", "MxAOD.Variables.Photon", {}, ignore);

    if (HG::isData()) ignore = {".SF_MV2c10_FixedCutBEff_60", ".SF_MV2c10_FixedCutBEff_70",
                              ".SF_MV2c10_FixedCutBEff_77", ".SF_MV2c10_FixedCutBEff_85",
                              ".Eff_MV2c10_FixedCutBEff_60", ".Eff_MV2c10_FixedCutBEff_70",
                              ".Eff_MV2c10_FixedCutBEff_77", ".Eff_MV2c10_FixedCutBEff_85",
                              ".InEff_MV2c10_FixedCutBEff_60", ".InEff_MV2c10_FixedCutBEff_70",
                              ".InEff_MV2c10_FixedCutBEff_77", ".InEff_MV2c10_FixedCutBEff_85",
                              ".HadronConeExclTruthLabelID"};

    declareOutputVariables(m_jetContainerName, "MxAOD.Variables.Jet", {}, ignore);

    if (HG::isData()) ignore = {".scaleFactor", ".truthLink", ".truthOrigin", ".truthType"};
    if(m_skimandslim==false){declareOutputVariables(m_elecContainerName  , "MxAOD.Variables.Electron", {}, ignore);}

    if (HG::isData()) ignore = {".scaleFactor",".truthOrigin", ".truthType", ".truthLink"};
    if(m_skimandslim==false)declareOutputVariables(m_muonContainerName  , "MxAOD.Variables.Muon"    , {}, ignore);
    if(m_skimandslim==false)declareOutputVariables("HGamMuonsInJets"    , "MxAOD.Variables.Muon"    , {}, ignore);

    //4. Truth Objects--------------------------
    if (HG::isMC())
    {
    	m_photonTruthContainerName = "HGam" + config()->getStr("TruthHandler.PhotonContainerName");
    	m_elecTruthContainerName   = "HGam" + config()->getStr("TruthHandler.ElectronContainerName");
    	m_muonTruthContainerName   = "HGam" + config()->getStr("TruthHandler.MuonContainerName");
        m_jetTruthContainerName    = "HGam" + config()->getStr("TruthHandler.JetContainerName");

    	declareOutputVariables(m_photonTruthContainerName, "MxAOD.Variables.TruthPhotons");
    	if(m_skimandslim==false)declareOutputVariables(m_elecTruthContainerName, "MxAOD.Variables.TruthElectrons");
    	if(m_skimandslim==false)declareOutputVariables(m_muonTruthContainerName, "MxAOD.Variables.TruthMuons");
    	declareOutputVariables(m_jetTruthContainerName, "MxAOD.Variables.TruthJets");
    	declareOutputVariables("HGam" + config()->getStr("TruthHandler.HiggsBosonContainerName"), "MxAOD.Variables.TruthHiggsBosons");
  	}


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode HcgamAnalysis::execute()
{
    //In oder all tools to work
  	HgammaAnalysis::execute();


    // For the cut-flow
    //the values are all declared in the header
    if (m_newFile && HG::isDAOD())
    {
    	addBookKeeping(getCutFlowHisto(), m_N_xAOD, m_N_DxAOD);

    	if (HG::isMC())
    	{
      	addBookKeeping(getCutFlowHisto(false), m_N_xAOD, m_N_DxAOD);
      	addBookKeeping(getCutFlowWeightedHisto(), m_sumw_xAOD, m_sumw_DxAOD, m_sumw2_xAOD, m_sumw2_DxAOD);
      	addBookKeeping(getCutFlowWeightedHisto(false), m_sumw_xAOD, m_sumw_DxAOD, m_sumw2_xAOD, m_sumw2_DxAOD);
    	}

    	m_newFile = false;
  	}

    // apply cuts. Returned value will be the last passed cut
    //this function checked the quality of event, photons, number of jets ... see header
    m_cutFlow = cutflow();

    // flag current event as a MC Dalitz event
    // (needed for cut-flow histograms)
    m_isDalitz = HG::isMC() && eventHandler()->isDalitz();

    // fill the cut-flow histograms up to tight selection
    //apply the weights
    //here we call the initial weights which are (mcWeight, pileupWeight, vertexWeight)
    //this weight doesnt have the info on the reco SF (see initial code)

    double wi = weightInitial();
    //loop over all applied cuts which is dictated by the analysis config
    //apply all cuts till the tight photon ID
    for (int cut = ALLEVTS; cut < m_cutFlow; ++cut){
    	if (cut <= GAM_TIGHTID)
    	{ fillCutFlow(CutEnum(cut), wi); }
     }


    //only write events that pass a given cut to the output
    //will write the output above m_skimCut (specified in the config file)
    if (m_skimCut >= 1 && m_cutFlow <= m_skimCut)
    { return EL::StatusCode::SUCCESS; }

    //Execute the function which will do the cuts, callculate variables
    //do the calibration and so on
    doReco();

    //fill the cut-flow histo after tight selection of photons
    //apply weights, which also include the SF
    //calculated after calling the function SetSelection() which is done in doReco()
    //weight (also saved in the ntuple in EventInfo.weight) - initialWeight()*RecoSF*TrigSF (no weights from JETS)
    double w = weight();

    for (int cut = ALLEVTS; cut < m_cutFlow; ++cut){
    		if (cut > GAM_TIGHTID)
    		{ fillCutFlow(CutEnum(cut), w); }
  	}

    if (HG::isMC() && (m_saveTruthObjects || m_saveTruthVars))
    { doTruth(); }


    // Write nominal EventInfo to output
    //all the weights, all the weights, some vertices variables
    //the weights here are the final weights
    eventHandler()->writeEventInfo();

    // Save all information written to output
    event()->fill();



  return EL::StatusCode::SUCCESS;
}


//===============================================================///
//////////////////////////HERE IS cutflow ()////////////////////////
//===============================================================///

// Returns value of the last cut passed in the cut sequence
HcgamAnalysis::CutEnum HcgamAnalysis::cutflow()
{
  	//Check if there are two good fakes. Needed so we dont slim the event at trigger.
  	m_goodFakeComb = false;

  	if (HG::isMC() && m_enableFakePhotons)
  	{
    double weightFakePhotons = 1;
    xAOD::PhotonContainer photonsWithFakes = getFakePhotons(weightFakePhotons);
    m_goodFakeComb = photonsWithFakes.size() > 1 ? true : false;

    if (m_goodFakeComb) { return PASSALL; }
    }
  //======================Here are the event cuts which are used for DATA and MC=====================
  //=================================================================================================

  //==== CUT 0 : DUBLICATE(only for data) ====
  static bool checkDuplicates = config()->getBool("EventHandler.CheckDuplicates");
  if (checkDuplicates && eventHandler()->isDuplicate()) { return DUPLICATE; }

  //==== CUT 1 : GRL ====
  static bool requireGRL = config()->getBool("EventHandler.CheckGRL");
  if (requireGRL && HG::isData() && !eventHandler()->passGRL(eventInfo())) { return GRL; }

  //==== CUT 2 : TRIGGER ====
  static bool requireTrigger = config()->getBool("EventHandler.CheckTriggers");
  if (requireTrigger && !eventHandler()->passTriggers()) { return TRIGGER; }

  //==== CUT 3 : QUALITY ====
  if (!(eventHandler()->passLAr(eventInfo()) &&
        eventHandler()->passTile(eventInfo()) &&
        eventHandler()->passSCT(eventInfo())))
  { return DQ; }

  //==== CUT 4 : VERTEX ====
  if (!eventHandler()->passVertex(eventInfo())) { return VERTEX; }

  // retrieve the photons
  //here we will determine the number of loose good photons
  //------all_photons - just the corrected photons
  //------loosePhotons - good photons, but passed only loose ID requirement
  xAOD::PhotonContainer   all_photons  = photonHandler() -> getCorrectedContainer();
  xAOD::PhotonContainer   loosePhotons = photonHandler() -> applyPreSelection(all_photons);
  int nloose = 0, namb = 0, nHV = 0;

  for (auto gam : all_photons) {
    if (photonHandler()->passOQCut(gam)       &&
        photonHandler()->passCleaningCut(gam) &&
        photonHandler()->passPtEtaCuts(gam)   &&
        photonHandler()->passPIDCut(gam, egammaPID::PhotonIDLoose))
    { ++nloose; }

    if (photonHandler()->passOQCut(gam)       &&
        photonHandler()->passCleaningCut(gam) &&
        photonHandler()->passPtEtaCuts(gam)   &&
        photonHandler()->passPIDCut(gam, egammaPID::PhotonIDLoose) &&
        photonHandler()->passAmbCut(gam))
    { ++namb; }

    if (photonHandler()->passOQCut(gam)       &&
        photonHandler()->passCleaningCut(gam) &&
        photonHandler()->passPtEtaCuts(gam)   &&
        photonHandler()->passPIDCut(gam, egammaPID::PhotonIDLoose) &&
        photonHandler()->passAmbCut(gam)       &&
        photonHandler()->passHVCut(gam))
    { ++nHV; }
  }

  //==== CUT 5 : 2 LOOSE GAMS ====
  //==== that have pT>25 GeV  and passed the cleanning and quality cuts ====
  if (nloose < 2) { return TWO_LOOSE_GAM; }


  //==== CUT 6 : PRESELECTED GAMS ===
  // - Require two loose photons that also pass e-gamma ambiguity (which is includes HV cuts ===
  static bool requireAmbiguity = config()->getBool("PhotonHandler.Selection.ApplyAmbiguityCut", false);
  if (requireAmbiguity && namb < 2) { return AMBIGUITY; }

  static bool requireHV = config()->getBool("PhotonHandler.Selection.ApplyHVCut", false);
  if (requireHV && nHV < 2) { return AMBIGUITY; }

  //==== CUT 7 : TRIGGER MATCHED GAMS ====
  static bool requireTriggerMatch = config()->getBool("EventHandler.CheckTriggerMatching", true);
  if (requireTriggerMatch && !passTriggerMatch(&loosePhotons)) { return TRIG_MATCH; }

  // Our *Higgs candidate photons* are the two, leading pre-selected photons
  // These are also the photons used to define the primary vertex
  xAOD::Photon *gam1 = loosePhotons[0], *gam2 = loosePhotons[1];

  //==== CUT 8 : 2 TIGHT GAMS ====
  static bool requireTight = config()->getBool("PhotonHandler.Selection.ApplyPIDCut", true);

  if (requireTight && (!photonHandler()->passPIDCut(gam1) ||
                       !photonHandler()->passPIDCut(gam2)))
  { return GAM_TIGHTID; }

  //==== CUT 9 : 2 ISOLATED GAMS ====
  static bool requireIso = config()->getBool("PhotonHandler.Selection.ApplyIsoCut", true);

  if (requireIso && (!photonHandler()->passIsoCut(gam1) ||
                     !photonHandler()->passIsoCut(gam2)))
  { return GAM_ISOLATION; }

  //==== CUT 10 : Relative pT cuts ====
  if (!passRelativePtCuts(loosePhotons)) { return RELPTCUTS; }

  //==== CUT 11 : Mass window cut ====
  if (!passMyyWindowCut(loosePhotons)) { return MASSCUT; }

  return PASSALL;
}


//===============================================================///
//////////////////////////HERE IS doReco ()////////////////////////
//===============================================================//

EL::StatusCode  HcgamAnalysis::doReco(bool isSys)
{
  	/////////////////////////////PHOTONS/////////////////////////////
  	//---------------------------------------------------------------//
  	// Get the corrected photons (calibrated..), apply the preselection (good, but loose ID)
  	m_allPhotons = photonHandler()->getCorrectedContainer();
  	m_preSelPhotons = photonHandler()->applyPreSelection(m_allPhotons);

  	// selected the two Higgs candidate photons (used for pointing)
  	m_selPhotons = xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
  	//Here we select 0,1 or 2 photon candidates (which are already preselected - loose,ptetacut,qaulity)
  	if (m_preSelPhotons.size()) { m_selPhotons.push_back(m_preSelPhotons[0]); }
  	if (m_preSelPhotons.size() > 1) { m_selPhotons.push_back(m_preSelPhotons[1]); }


  	// The below logic is for use in background side-band studies
  	// For example, the loose-not-tight background category, where a leading loose photon
  	// is still considered as part of the diphoton system
  	// When checking for isPassed events, all photons are guaranteed to be tight
  	//////this piece of code inherited from other code////
  	////can be useful in the future, so far disabled in the config/////
  	///////////////////////////////////////////////////////////////////
  	if (m_allowMoreThanTwoPhotons){
    	// any tight isolated additional photon, is also treated as a photon
    	for (size_t i = 2; i < m_preSelPhotons.size(); ++i){
      		xAOD::Photon *gam = m_preSelPhotons[i];

      		if (photonHandler()->passIsoCut(gam) &&
          		photonHandler()->passPIDCut(gam))
      		{ m_selPhotons.push_back(gam); }
    	}
  	}//end of if more than 2 photons

  	// If we enable fake photons then we replace the existing photon selection with the fake combinations.
  	if (m_enableFakePhotons){
    	double weightFakePhotons = 1;
    	xAOD::PhotonContainer photonsWithFakes(SG::VIEW_ELEMENTS);

    	if (m_goodFakeComb){
      		photonsWithFakes = getFakePhotons(weightFakePhotons);
      		eventHandler()->storeVar<float>("weightFakePhotons", weightFakePhotons);
      		m_preSelPhotons = photonsWithFakes;
      		m_selPhotons    = photonsWithFakes;
    	}
  	}
  	///////////////////////////////////////////////////////////////////

  	/////////////////////////////JETS//////////////////////////////////
  	//---------------------------------------------------------------//
  	//Set other containers:
  	m_allJets = jetHandler()->getCorrectedContainer();
  	m_selJets = jetHandler()->applySelection(m_allJets); //good jets with all the cuts, JVT,JFT - used for the analysis

  	/////////////////////////////PFlow JETS//////////////////////////////////
  	//set the container and make the cuts on jets
  	xAOD::JetContainer allPFlowJets = jetHandlerPFlow()->getCorrectedContainer();
    xAOD::JetContainer selPFlowJets = jetHandlerPFlow()->applySelection(allPFlowJets);

  	/////////////////////////////Electrons/////////////////////////////
  	//---------------------------------------------------------------//
  	//electrons and muons are used for the overlap removal and the lepton veto////
  	m_allElectrons = electronHandler()->getCorrectedContainer();
  	m_selElectrons = electronHandler()->applySelection(m_allElectrons);

  	/////////////////////////////MUONS/////////////////////////////////
  	//---------------------------------------------------------------//
  	m_allMuons = muonHandler()->getCorrectedContainer();
  	xAOD::MuonContainer dirtyMuons = muonHandler()->applySelection(m_allMuons);
  	//clean the muons
  	//the function removes muons marcked as isBadMuon()
  	//this function returns true if a CB muon fails some loose quaility
  	//requirements designed to remove pathological tracks.
  	m_selMuons = muonHandler()->applyCleaningSelection(dirtyMuons);

    ////////////////////DOING THE OVERLAP HANDLER WITH GOOD VARIABLES//////////////
    //--------------------------------------------------------------------------//
  	// Removes overlap with candidate diphoton system, and any additional tight photons (if option set)
  	//check the overlap strategy in the header
  	// Removes overlap with candidate diphoton system, and any additional tight photons (if option set)
    overlapHandler()->removeOverlap(m_selPhotons, m_selJets, m_selElectrons, m_selMuons);


    // Special overlap removal for PFlow jets
  	xAOD::ElectronContainer pflowElecs = m_selElectrons;
  	xAOD::MuonContainer pflowMuons = m_selMuons;
  	overlapHandler()->removeOverlap(m_selPhotons, selPFlowJets, pflowElecs, pflowMuons);


    //---------------------------------------------------------------//
    //this is for the JVT weights
    //---------------------------------------------------------------//
  	// Save JVT weight (needs special overlap removal)
  	m_jvtJets = jetHandler()->applySelectionNoJvt(m_allJets);
  	xAOD::ElectronContainer jvtElecs = m_selElectrons;
  	xAOD::MuonContainer jvtMuons = m_selMuons;
  	overlapHandler()->removeOverlap(m_selPhotons, m_jvtJets, jvtElecs, jvtMuons);

	//Initialise the weights + some other weight for the categories which we dont need in our analysis
  	setSelectedObjects(&m_selPhotons, &m_selElectrons, &m_selMuons, &m_selJets, nullptr , &m_jvtJets);

    hcgamgamTool()->saveHCInfo(m_selPhotons,m_selJets, m_jvtJets, m_selMuons, m_selElectrons);


/*
    /////////////////////////////MAKING the LEPTON VETO////////////////
  	//---------------------------------------------------------------//
  	//Apply the lepton cleaning to remove them - so they will not look like jets (Do we need this one?)
	//Muons
	xAOD::MuonContainer muonsPassing(SG::VIEW_ELEMENTS);
    // Apply muon requirements
    std::copy_if(m_selMuons.begin(), m_selMuons.end(), std::back_inserter(muonsPassing), [this](const xAOD::Muon * muon) {
      return (muon->pt() >= m_minMuonPT * HG::GeV);
    });
    //Electrons
    xAOD::ElectronContainer electronsPassing(SG::VIEW_ELEMENTS);
    // Apply electron requirements
    std::copy_if(m_selElectrons.begin(), m_selElectrons.end(), std::back_inserter(electronsPassing), [this](const xAOD::Electron * electron) {
      return (electron->pt() >= m_minElectronPT * HG::GeV);
    });

    //Remove the leptons
    if (muonsPassing.size() > 0) { return EL::StatusCode::SUCCESS;}
    if (electronsPassing.size() > 0) { return EL::StatusCode::SUCCESS;}
*/


    xAOD::JetContainer jvtJets20(SG::VIEW_ELEMENTS),jvtJets25(SG::VIEW_ELEMENTS),jvtJets30(SG::VIEW_ELEMENTS),jvtJets35(SG::VIEW_ELEMENTS),jvtJets50(SG::VIEW_ELEMENTS);

  	for (auto jet : m_jvtJets){
  	    //making eta cut
  	    if (fabs(jet->eta()) > 2.5) { continue; }
  	    ///Making cuts pn pt and jet containers
    	if (jet->pt() < 20.0 * HG::GeV)
    	{ continue; }
    	jvtJets20.push_back(jet);

    	if (jet->pt() < 25.0 * HG::GeV)
    	{ continue; }
    	jvtJets25.push_back(jet);

    	if (jet->pt() < 30.0 * HG::GeV)
    	{ continue; }
    	jvtJets30.push_back(jet);

    	if (jet->pt() < 35.0 * HG::GeV)
    	{ continue; }
    	jvtJets35.push_back(jet);

    	if (jet->pt() < 50.0 * HG::GeV)
    	{ continue; }
    	jvtJets50.push_back(jet);
  	}
	///Saving weights per event.. this includes already accounting for the inefficiency and efficiency
    eventHandler()->storeVar<float>("weightJvt_20", HG::JetHandler::multiplyJvtWeights(&jvtJets20));
    eventHandler()->storeVar<float>("weightJvt_25", HG::JetHandler::multiplyJvtWeights(&jvtJets25));
    eventHandler()->storeVar<float>("weightJvt_30", HG::JetHandler::multiplyJvtWeights(&jvtJets30));
  	eventHandler()->storeVar<float>("weightJvt_35", HG::JetHandler::multiplyJvtWeights(&jvtJets35));
  	eventHandler()->storeVar<float>("weightJvt_50", HG::JetHandler::multiplyJvtWeights(&jvtJets50));



    if (not isSys){
    	writeNominalOnly();

    	if (m_saveDetailed) { writeDetailed(); }

    	if (m_saveObjects){
      			CP_CHECK("execute()", photonHandler()->writeContainer(m_selPhotons));
      			CP_CHECK("execute()", jetHandler()->writeContainer(m_selJets));
                CP_CHECK("execute()", jetHandlerPFlow()->writeContainer(selPFlowJets));
                CP_CHECK("execute()", muonHandler()->writeContainer(m_selMuons));
                CP_CHECK("execute()", electronHandler()->writeContainer(m_selElectrons));
    	}
  	}//write the nominal values

  for (auto photon : m_selPhotons) {
    if (photon->pt() != photon->pt()) {
      HG::fatal("Photons have NaN pT?");
    }
  }

  //  doTruthMatch(m_selJets);
  // Adds all event variables (weight, category, isPassed, and pT_yy etc.)
  // to the TEvent output stream
  HG::VarHandler::getInstance()->write();

  return EL::StatusCode::SUCCESS;
}


void HcgamAnalysis::writeNominalOnly()
{

  // Basic event selection flags
  var::isPassedBasic.setValue(m_goodFakeComb ? true : eventHandler()->pass());
  var::isPassed.setValue(m_goodFakeComb ? true : var::isPassedBasic() && pass(&m_selPhotons, &m_selElectrons, &m_selMuons, &m_selJets));
  var::cutFlow.setValue(m_cutFlow);

  if (HG::isMC()) { var::isDalitzEvent.setValue(m_isDalitz); }

  eventHandler()->mu();
  eventHandler()->runNumber();

  // Additional cut flow granularity
  int Nloose = m_preSelPhotons.size();
  bool passTrigMatch = passTriggerMatch(&m_preSelPhotons);
  bool passIso = false, passPID = false;

  if (Nloose >= 2) {
    xAOD::Photon *y1 = m_preSelPhotons[0], *y2 = m_preSelPhotons[1];
    //if m_goofFakeComb = false, returns the values for the isocuts and PIDcuts (true or false), otherwise returns true
    passIso = m_goodFakeComb ? true : photonHandler()->passIsoCut(y1) && photonHandler()->passIsoCut(y2);
    passPID = m_goodFakeComb ? true : photonHandler()->passPIDCut(y1) && photonHandler()->passPIDCut(y2);
  }


    //So far I will just cut on all jets, later we can consider to distinguish between the central jet and forward jets
    ///where on central jets we will apply the B tagging.
   //yet the B tagging and the c tagging still needs to be properly implemented
   //the cuts performed onlt on the selected jets, wich passed all the requirements
  int  jet_n = m_selJets.size();
  decorateJet(m_selJets);



  eventHandler()->storeVar<char>("isAtLeast1Jet",  jet_n >= 1);//at least 1 jet in the event
  eventHandler()->storeVar<char>("isPassedPreselection", Nloose >= 2);
  eventHandler()->storeVar<char>("isPassedTriggerMatch", passTrigMatch);
  eventHandler()->storeVar<char>("isPassedPID", passPID);
  eventHandler()->storeVar<char>("isPassedIsolation", passIso);
  eventHandler()->storeVar<char>("isPassedRelPtCuts", passRelativePtCuts(m_preSelPhotons));
  eventHandler()->storeVar<char>("isPassedMassCut", passMyyWindowCut(m_preSelPhotons));
  eventHandler()->storeVar<char>("isJetClean",passJetEventCleaning());




// Masses with different PV definitions
  const CP::PhotonPointingTool *pointingTool = photonHandler()->getPointingTool();
  if (pointingTool) {
    if (m_cutFlow > AMBIGUITY) {
      xAOD::PhotonContainer leadPhotons = m_preSelPhotons;
      leadPhotons.resize(2);

      // Store m_yy using hardest vertex
      eventHandler()->storeVar<float>("m_yy_hardestVertex",  pointingTool->getCorrectedMass(leadPhotons, eventHandler()->hardestVertexZ()));

      if (HG::isMC())
      { eventHandler()->storeVar<float>("m_yy_truthVertex", pointingTool->getCorrectedMass(leadPhotons, truthHandler()->vertexZ())); }

      // Store m_yy using zCommon
      eventHandler()->storeVar<float>("m_yy_zCommon",  pointingTool->getCorrectedMass(leadPhotons, xAOD::PVHelpers::getZCommonAndError(eventInfo(), &leadPhotons).first));
      var::zCommon.setValue(xAOD::PVHelpers::getZCommonAndError(eventInfo(), &leadPhotons).first);
    } else {
      eventHandler()->storeVar<float>("m_yy_hardestVertex", -99);

      if (HG::isMC())
      { eventHandler()->storeVar<float>("m_yy_truthVertex", -99); }

      eventHandler()->storeVar<float>("m_yy_zCommon", -99);
    }
  }

  // Vertex information
  eventHandler()->numberOfPrimaryVertices();
  eventHandler()->selectedVertexZ();
  eventHandler()->selectedVertexPhi();
  eventHandler()->selectedVertexSumPt2();
  eventHandler()->hardestVertexZ();
  eventHandler()->hardestVertexPhi();
  eventHandler()->hardestVertexSumPt2();
  eventHandler()->pileupVertexSumPt2(); // also sets pileupVertexZ internally

  if (HG::isMC()) { truthHandler()->vertexZ(); }

  const xAOD::VertexContainer *vertices = nullptr;

  if (event()->contains<xAOD::VertexContainer>("PrimaryVertices")){
    if (event()->retrieve(vertices, "PrimaryVertices").isFailure())
    { HG::fatal("Error retrieving PrimaryVertices, exiting"); }

    std::vector<float> verticesZ;
    std::vector<float> verticesScore;
    static SG::AuxElement::ConstAccessor<float> vertexScore("vertexScore");

    if (vertices->size() == 1) {
      // DxAODs in p2669 have issues with only dummy vertex and vertex score decoration
      verticesZ.push_back(-999);
      verticesScore.push_back(-99);
    } else {
      for (auto vertex : *vertices) {
        verticesZ.push_back(vertex->z());
        verticesScore.push_back(vertexScore(*vertex));
      }
    }

    eventHandler()->storeVar<std::vector<float> >("PrimaryVerticesZ", verticesZ);
    eventHandler()->storeVar<std::vector<float> >("PrimaryVerticesScore", verticesScore);
  }


  // Bunch train information
  eventHandler()->bunchDistanceFromFront();
  eventHandler()->bunchGapBeforeTrain();


  // Add MC only variables
  if (HG::isMC()) {
    truthHandler()->catCoup();
    truthHandler()->isVyyOverlap();

    if (config()->isDefined(TString::Format("CrossSection.%d", eventInfo()->mcChannelNumber()))) {
      double xs = getCrossSection(), kf = 1.0, ge = 1.0;

      if (config()->isDefined(TString::Format("kFactor.%d", eventInfo()->mcChannelNumber())))
      { kf = getKFactor(); }

      if (config()->isDefined(TString::Format("GeneratorEfficiency.%d", eventInfo()->mcChannelNumber())))
      { ge = getGeneratorEfficiency(); }

      eventHandler()->storeVar<float>("crossSectionBRfilterEff", xs * kf * ge);
    } else {
      eventHandler()->storeVar<float>("crossSectionBRfilterEff", -1);
    }
  }

  writeNominalOnlyVars();

}

void HcgamAnalysis::writeNominalOnlyVars(bool truth)
{
  // Truth and reco vars
  var::pT_y1        .addToStore(truth);
  var::pT_y2        .addToStore(truth);
  var::E_y1         .addToStore(truth);
  var::E_y2         .addToStore(truth);
  var::DR_y_y       .addToStore(truth);

  var::N_j          .addToStore(truth);
  var::pT_j1        .addToStore(truth);
  var::pT_j2        .addToStore(truth);
  var::DRmin_y_j    .addToStore(truth);


  if (not truth) {
    var::DRmin_y_j_2  .addToStore(truth);
    var::m_alljet     .addToStore(truth);
    var::m_alljet_30  .addToStore(truth);
    var::pT_y1        .addToStore(truth);
    var::pT_y2        .addToStore(truth);
    var::DR_y_y       .addToStore(truth);
    var::N_j          .addToStore(truth);
    var::m_yy         .addToStore(truth);

  }

}




void HcgamAnalysis::writeDetailed()
{

  // Just calling these adds the variables to the TStore
  eventHandler()->centralEventShapeDensity();
  eventHandler()->forwardEventShapeDensity();

  writeDetailedVars();
}

void HcgamAnalysis::writeDetailedVars(bool truth)
{
  var::Dphi_y_y     .addToStore(truth);
  var::yAbs_j1      .addToStore(truth);
  var::yAbs_j2      .addToStore(truth);
  var::pT_yyj       .addToStore(truth);
  var::yAbs_yy      .addToStore(truth);
  var::pT_yy        .addToStore(truth);
  var::m_yy         .addToStore(truth);
  var::cosTS_yy		.addToStore(truth);
  var::Dphi_y_y		.addToStore(truth);
  var::m_yyj		.addToStore(truth);


}


EL::StatusCode  HcgamAnalysis ::doTruth()
{
  // Truth particles
  xAOD::TruthParticleContainer all_photons   = truthHandler()->getPhotons();
  xAOD::TruthParticleContainer all_electrons = truthHandler()->getElectrons();
  xAOD::TruthParticleContainer all_dms       = truthHandler()->getDM();
  xAOD::TruthParticleContainer all_muons     = truthHandler()->getMuons();
  xAOD::JetContainer           all_jets      = truthHandler()->getJets();
  xAOD::MissingETContainer     all_met       = truthHandler()->getMissingET();
  xAOD::TruthParticleContainer all_higgs     = truthHandler()->getHiggsBosons();

  // Apply fiducial selections to all containers
  xAOD::TruthParticleContainer photons   = truthHandler()->applyPhotonSelection(all_photons);
  xAOD::TruthParticleContainer electrons = truthHandler()->applyElectronSelection(all_electrons);
  xAOD::TruthParticleContainer muons     = truthHandler()->applyMuonSelection(all_muons);
  xAOD::JetContainer           jets      = truthHandler()->applyJetSelection(all_jets);
  xAOD::JetContainer           bjets     = truthHandler()->applyBJetSelection(jets);
  xAOD::JetContainer           cjets     = truthHandler()->applyCJetSelection(jets);
  xAOD::MissingETContainer     met       = truthHandler()->applyMissingETSelection(all_met);

  // remove truth jets that are from electrons or photons
  truthHandler()->removeOverlap(photons, jets, electrons, muons);

  // Save truth containers, if configured
  if (m_saveTruthObjects){
    truthHandler()->writePhotons(all_photons);
    truthHandler()->writeElectrons(all_electrons);
    truthHandler()->writeMuons(all_muons);
    truthHandler()->writeJets(all_jets);
    truthHandler()->writeMissingET(met);
    truthHandler()->writeHiggsBosons(all_higgs);
    truthHandler()->writeTruthEvents();

    addTruthLinks(m_photonContainerName.Data(), m_photonTruthContainerName.Data());
    addTruthLinks(m_elecContainerName.Data(), m_elecTruthContainerName.Data());
  }

  HG::VarHandler::getInstance()->setTruthContainers(&all_photons, &electrons, &muons, &jets, &met);
  HG::VarHandler::getInstance()->setHiggsBosons(&all_higgs);

  // Adds event-level variables to TStore (this time using truth containers)
  bool truth = true;

  if (m_saveTruthVars){

    writeNominalOnlyVars(truth);
    if (m_saveDetailed)
    { writeDetailedVars(truth); }

    truthHandler()->passFiducial(&all_photons); // calls passFiducialKinOnly internally

    truthHandler()->centralEventShapeDensity();
    truthHandler()->forwardEventShapeDensity();


    var::pT_h1.addToStore(truth);
    var::pT_h2.addToStore(truth);
    var::y_h1.addToStore(truth);
    var::y_h2.addToStore(truth);
    var::m_h1.addToStore(truth);
    var::m_h2.addToStore(truth);

    //saving b and c jets with pT>30 GeV and within eta 2.5
    xAOD::JetContainer bjets25_y25(SG::VIEW_ELEMENTS);
    xAOD::JetContainer cjets25_y25(SG::VIEW_ELEMENTS);

    xAOD::JetContainer jets25_y25(SG::VIEW_ELEMENTS);
    xAOD::JetContainer jets30_y25(SG::VIEW_ELEMENTS);


    for (auto jet : bjets){
      if (fabs(jet->eta()) > 2.5) { continue; }
      if (jet->pt() < 25.0 * HG::GeV) { continue; }
      bjets25_y25.push_back(jet);
    }


    for (auto jet : cjets){
      if (jet->pt() < 25.0 * HG::GeV) { continue; }
      if (fabs(jet->eta()) > 2.5) { continue; }
      cjets25_y25.push_back(jet);
    }

    eventHandler()->storeTruthVar<int>("N_bjet25", bjets25_y25.size());
    eventHandler()->storeTruthVar<int>("N_cjet25", cjets25_y25.size());


 	//Loop over jets
    for (auto jet : jets) {
      if (fabs(jet->eta()) > 2.5 || jet->pt() < 25 * HG::GeV)  { continue; }
      	jets25_y25.push_back(jet);
      if (fabs(jet->eta()) > 2.5 || jet->pt() < 30 * HG::GeV)  { continue; }
      	jets30_y25.push_back(jet);
    }

    eventHandler()->storeTruthVar<int>("N_jet25", jets25_y25.size());
    eventHandler()->storeTruthVar<int>("N_jet30", jets30_y25.size());



  }

  // Adds all event variables to the TEvent output stream
  HG::VarHandler::getInstance()->writeTruth();

  return EL::StatusCode::SUCCESS;
}





//Inherited from the HGamTool////////////////
////////////////////////////////////////////
TH1F *HcgamAnalysis::makeCutFlowHisto(int id, TString suffix)
{
  int Ncuts = s_cutDescs.size();

  // create meaningful name of the cutflow histo
  TString name(Form("CutFlow_%s%d", HG::isData() ? "Run" : "MC", std::abs(id)));

  bool hasMCname = HG::isMC() && config()->isDefined(Form("SampleName.%d", std::abs(id)));

  if (hasMCname) {
    name = Form("CutFlow_%s", getMCSampleName(std::abs(id)).Data());
  }

  if (HG::isMC() && !hasMCname && config()->getStr("SampleName", "sample") != "sample")
  { name = "CutFlow_" + config()->getStr("SampleName"); }

  name += suffix;

  // maybe should make sure we don't switch directory?
  TDirectory *dir = gDirectory;
  TFile *file = wk()->getOutputFile("MxAOD");
  TH1F *h = new TH1F(name, name, Ncuts, 0, Ncuts);
  h->SetDirectory(file); // probably not needed

  for (int bin = 1; bin <= Ncuts; ++bin)
  { h->GetXaxis()->SetBinLabel(bin, s_cutDescs[bin - 1]); }

  dir->cd();
  return h;
}


void HcgamAnalysis::fillCutFlow(CutEnum cut, double w)
{
  getCutFlowHisto()->Fill(cut);

  if (HG::isData()) { return; }

  getCutFlowWeightedHisto()->Fill(cut, w);

  if (m_isDalitz) { return; }

  getCutFlowHisto(false)->Fill(cut);
  getCutFlowWeightedHisto(false)->Fill(cut, w);
}

EL::StatusCode HcgamAnalysis::finalize()
{
  printf("\nEvent selection cut flow:\n");
  printCutFlowHistos();

  // Write the output to file
  HgammaAnalysis::finalize();

  SafeDelete(m_hcgamgamTool);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HcgamAnalysis::fileExecute()
{
  HgammaAnalysis::fileExecute();

  // Tell the code a new file has just been opened
  m_newFile = true;

  TTree *MetaData = dynamic_cast<TTree *>(wk()->inputFile()->Get("MetaData"));

  if (MetaData == nullptr)
  { HG::fatal("Couldn't find MetaData TTree in event, is this a proper xAOD file? Exiting."); }

  // The isDAOD flag set in HgammaAnalysis::changeInput is not available here, so we get it maually.
  bool tmp_isDAOD = false;

  for (auto branch : * MetaData->GetListOfBranches()) {
    if (TString(branch->GetName()).Contains("StreamDAOD")) {
      tmp_isDAOD = true;
      break;
    }
  }

  if (tmp_isDAOD) {
    // If we get here, this is a DxAOD

    // 1. Check if there if the incomplete book-keeper object has entreies (that would be bad!)
    const xAOD::CutBookkeeperContainer *incompleteCBC = nullptr;
    EL_CHECK("fileExecute()", wk()->xaodEvent()->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers"));

    if (incompleteCBC->size() != 0) {
      // fatal or error? let's start with a hard fatal!
      // HG::fatal("Issue with DxAOD book keeper. It's incomplete. File corrupted?");
      Warning("fileExecute()",
              "Issue with DxAOD book keeper. It's incomplete. File corrupted? %s %s",
              "If this is data, this is known to happen (but not understood).",
              "If this is MC, this is expected to happen, and can probably be ignored.");
    }

    // 2. now get the actual bookkeeper
    const xAOD::CutBookkeeperContainer *completeCBC = nullptr;
    EL_CHECK("fileExecute()", wk()->xaodEvent()->retrieveMetaInput(completeCBC, "CutBookkeepers"));

    int maxAcycle = -1, maxDcycle = -1;

    for (auto cbk : *completeCBC) {
      Info("fileExecute()", "  Book keeper name=%s, inputStream=%s, cycle=%d, nAcceptedEvents=%d", cbk->name().c_str(), cbk->inputStream().c_str(), cbk->cycle(), (int)cbk->nAcceptedEvents());

      if (cbk->name().empty()) { continue; }

      // Use the DxAOD numbers from the largest cycle
      if (TString(cbk->name()).Contains("HIGG1D1") && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxDcycle) {
        maxDcycle     = cbk->cycle();
        m_N_DxAOD     = cbk->nAcceptedEvents();
        m_sumw_DxAOD  = cbk->sumOfEventWeights();
        m_sumw2_DxAOD = cbk->sumOfEventWeightsSquared();
      }


      // Use the xAOD numbers from the largest cycle
      if (cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxAcycle) {
        maxAcycle    = cbk->cycle();
        m_N_xAOD     = cbk->nAcceptedEvents();
        m_sumw_xAOD  = cbk->sumOfEventWeights();
        m_sumw2_xAOD = cbk->sumOfEventWeightsSquared();
      }
    }

    Info("fileExecute()", "Book keeper anticipates %i events in current input file (%i in parent xAOD)", m_N_DxAOD, m_N_xAOD);
  }

  return EL::StatusCode::SUCCESS;
}

void HcgamAnalysis::printCutFlowHistos()
{
  for (auto entry : m_cFlowHistos) {
    printf("\n%s %d cut-flow%s\n", HG::isMC() ? "MC sample" : "Data run",
           std::abs(entry.first), entry.first > 0 ? ", excluding Dalitz events" : "");
    printCutFlowHisto(entry.second, 0);
  }

  for (auto entry : m_cFlowHistosWeighted) {
    printf("\n%s %d cut-flow, weighted events%s\n", HG::isMC() ? "MC sample" : "Data run",
           std::abs(entry.first), entry.first > 0 ? ", excluding Dalitz events" : "");
    printCutFlowHisto(entry.second, 2);
  }

  printf("\n");
}

void HcgamAnalysis::printCutFlowHisto(TH1F *h, int Ndecimals)
{
  TString format("  %-24s%10.");
  format += Ndecimals;
  format += "f%11.2f%%%11.2f%%\n";
  int all_bin = h->FindBin(ALLEVTS);
  printf("  %-24s%10s%12s%12s\n", "Event selection", "Nevents", "Cut rej.", "Tot. eff.");

  for (int bin = 1; bin <= h->GetNbinsX(); ++bin) {
    double ntot = h->GetBinContent(all_bin), n = h->GetBinContent(bin), nprev = h->GetBinContent(bin - 1);
    TString cutName(h->GetXaxis()->GetBinLabel(bin));
    cutName.ReplaceAll("#it{m}_{#gamma#gamma}", "m_yy");

    if (bin == 1 || nprev == 0 || n == nprev)
    { printf(format.Data(), cutName.Data(), n, -1e-10, n / ntot * 100); }
    else // if the cut does something, print more information
    { printf(format.Data(), cutName.Data(), n, (n - nprev) / nprev * 100, n / ntot * 100); }
  }
}


 // _______________________________________________________________________________________
 //Decorate jets with DL1 scores
  void HcgamAnalysis::decorateJet(xAOD::JetContainer &jets)
  {

    for (auto jet: jets){
        //Extracting the DL1 scores (is it really THE DL1 scores that are for the c tagging?)
        const xAOD::BTagging* ctag =jet->btagging();
        double jet_pb = -999.;
        double jet_pc = -999.;
        double jet_pu = -999.;
        ctag->pb("DL1", jet_pb);
        ctag->pc("DL1", jet_pc);
        ctag->pu("DL1", jet_pu);

        jet->auxdata<double>("DL1_pu") = jet_pu;
        jet->auxdata<double>("DL1_pb") = jet_pb;
        jet->auxdata<double>("DL1_pc") = jet_pc;


		int truthLabel = HG::isMC() ? jet->auxdata<int>("HadronConeExclTruthLabelID") : -99;
        jet->auxdata<int>("HadronTruthLabel") = truthLabel;


    }


	//saving b and c jets with pT>30 GeV and within eta 2.5
    xAOD::JetContainer jets25_y25(SG::VIEW_ELEMENTS);
    xAOD::JetContainer jets30_y25(SG::VIEW_ELEMENTS);

    for (auto jet : jets){
      if (fabs(jet->eta()) > 2.5) { continue; }
      if (jet->pt() < 25.0 * HG::GeV) { continue; }
      jets25_y25.push_back(jet);
      if (jet->pt() < 25.0 * HG::GeV) { continue; }
      jets30_y25.push_back(jet);

    }

    eventHandler()->storeVar<int>("Ncent_j25", jets25_y25.size());
    eventHandler()->storeVar<int>("Ncent_j35", jets30_y25.size());




  }

 // _______________________________________________________________________________________

//Calculate the weight
// Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
double HcgamAnalysis::weightJVT(xAOD::JetContainer &jets_noJVT)
  {

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > 2.5) { continue; }
      if (jet->pt() < 25 * HG::GeV) { continue; }
      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }


void HcgamAnalysis::doTruthMatch(xAOD::JetContainer &jets_sel)
{

	//Get the truth particle container

	xAOD::TruthParticleContainer all_tru_photons   = truthHandler()->getPhotons();
  	xAOD::TruthParticleContainer all_tru_electrons = truthHandler()->getElectrons();
  	xAOD::TruthParticleContainer all_tru_muons     = truthHandler()->getMuons();
  	xAOD::JetContainer           all_tru_jets      = truthHandler()->getJets();
  	xAOD::MissingETContainer     all_tru_met       = truthHandler()->getMissingET();


  	// Apply fiducial selections to all containers
  	xAOD::TruthParticleContainer tru_photons   = truthHandler()->applyPhotonSelection(all_tru_photons);
  	xAOD::TruthParticleContainer tru_electrons = truthHandler()->applyElectronSelection(all_tru_electrons);
  	xAOD::TruthParticleContainer tru_muons     = truthHandler()->applyMuonSelection(all_tru_muons);
    xAOD::JetContainer           tru_jets      = truthHandler()->applyJetSelection(all_tru_jets);
    xAOD::MissingETContainer     tru_met       = truthHandler()->applyMissingETSelection(all_tru_met);

    // remove truth jets that are from electrons or photons
    //truthHandler()->removeOverlap(tru_photons, tru_jets, tru_electrons, tru_muons);


    //HG::VarHandler::getInstance()->setTruthContainers(&all_tru_photons, &all_tru_electrons, &all_tru_muons, &all_tru_jets, &all_tru_met);

    //Open the truthparticle container
	const xAOD::TruthParticleContainer *truthPart = truthHandler()->getTruthParticles();
  	HG::TruthContainer Bs = HG::getBHadrons(truthPart, 5.0 * HG::GeV);
    HG::TruthContainer Ds = HG::getDHadrons(truthPart, 5.0 * HG::GeV);


  	for(auto recoj: jets_sel){
    	bool hasTruthJet = false;
    	float dR_min = 999.;
    	bool isC = false; bool isB = false; bool isL = false;

    	for(auto Trujets: all_tru_jets){
    			if(Trujets->pt() > 0 && Trujets->pt() < 10 * HG::GeV){ continue; }
				float R = dr(Trujets->phi(),Trujets->eta(),recoj->phi(),recoj->eta());

				if(R < 0.2 && R < dR_min){
					hasTruthJet = true;
					dR_min=R;
					isC = false; isB = false; isL = false;
					if(HG::minDRrap(Trujets, Bs) < 0.4){isC = true;}
					else if (HG::minDRrap(Trujets, Ds) < 0.4) {isB = true;}
					else {isL = true;}

				}
		}//end of truth loop
		bool truth_b = false; bool truth_l = false; bool truth_c = false;
		std::cout<<"Saving the jets "<<std::endl;
		if(hasTruthJet && isB) {truth_b = true;} recoj->auxdata<char>("recoj_matchC") = truth_b;
		if(hasTruthJet && isC) {truth_c = true;} recoj->auxdata<char>("recoj_matchB") = truth_c;
		if(hasTruthJet && isL) {truth_l = true;} recoj->auxdata<char>("recoj_matchL") = truth_l;

	}//end of reco jet loop


}


double HcgamAnalysis::dr(double phi1, double eta1, double phi2, double eta2)
{
  double deta=eta1-eta2;
  double dphi=phi1-phi2;
  while (dphi < -TMath::Pi() ) dphi += 2*TMath::Pi();
  while (dphi >  TMath::Pi() ) dphi -= 2*TMath::Pi();
  return std::sqrt(deta*deta+dphi*dphi);
}
