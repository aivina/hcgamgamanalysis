///////////////////////////////////////////
// H+c selection Tool
// Tool performs all the yy+c selection
// All info decorated to MxAODs
// @author: anna.ivina@cern.ch
///////////////////////////////////////////

// STL includes
#include <algorithm>

// ATLAS/ROOT includes
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"



// Local includes
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"
#include "hcgamgamanalysis/HcgamgamTool.h"

namespace HG {
  // _______________________________________________________________________________________
  HcgamgamTool::HcgamgamTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_minMuonPT(10)
    , m_minElectronPT(10)
	, m_jet_eta_max(2.5)
	, m_jet_pt(30)
	, m_Njets(2)
	, m_dRcut(1.5)
	, m_truthjet_rapidity_max(2.5)
  {}

  // _______________________________________________________________________________________
  HcgamgamTool::~HcgamgamTool()
  {
  }


  // _______________________________________________________________________________________
  HG::EventHandler *HcgamgamTool::eventHandler()
  {
    return m_eventHandler;
  }


   // _______________________________________________________________________________________
  HG::TruthHandler *HcgamgamTool::truthHandler()
  {
    return m_truthHandler;
  }


  // _______________________________________________________________________________________
  EL::StatusCode HcgamgamTool::initialize(Config &config)
  {
    // Set default cut values etc here...
    // Almost all of these are set hard-coded because this will be used by the nominal MxAOD production
    // Also safer :)

    m_minMuonPT = config.getNum("HcgamgamTool.MinMuonPt", 10.0); //! minimum muon pt (in GeV!!! ) for lepton veto - not usi ng for now
    ATH_MSG_INFO("MinMuonPt.................................. " << m_minMuonPT);

    m_minElectronPT = config.getNum("HcgamgamTool.MinElectronPt", 10.0); //! minimum electron pt (in GeV!!!) for lepton veto - not using for now
    ATH_MSG_INFO("MinElectronPt.............................. " << m_minElectronPT);

    m_jet_eta_max = config.getNum("HcgamgamTool.MaxJetEta", 2.5); //! Max jet eta in abs
    ATH_MSG_INFO("MaxJetEta........................... " << m_jet_eta_max);

    m_jet_pt     = config.getNum("HcgamgamTool.LooseJetpT", 30); //! Loose jet pT cut in GeV - leave this for now
    ATH_MSG_INFO("LooseJetpT........................... " << m_jet_pt);

    m_Njets = config.getNum("HcgamgamTool.NJetInEvent", 2); //! How many jets you want to use in your analysis
    ATH_MSG_INFO("NJetInEvent........................... " << m_Njets);

    m_dRcut = config.getNum("HcgamgamTool.dRcutJetPho", 1.5); //! Dr cut between a leading jet and a photon
    ATH_MSG_INFO("dRCutJetPhotons........................... " << m_dRcut);

    m_tagger = config.getStr("HcgamgamTool.Tagger", "DL1");//The name of the Tagger for Ctagging - DL1 is default by now
    ATH_MSG_INFO("TaggerName........................... " << m_tagger);


	//In current version CDI files - 2019-21-13TeV-MC16-CDI-2019-10-07_v1.root in DL1/AntiKt4EMTopoJets_BTagging201810 we have 3 WP for c-tagging
	//CTag_Loose - DL1 cut 0.61456, fb - 0.18
	//CTag_Tight - DL1 cut 1.3, fb - 0.08
	//Ctag_Tight_Veto_MV2c10_FixedBEff_70 - require jets !b @ 70% with MV2c10_FixedBEff_70 and DL1 cut > 1.3 and fb - 0.08


    return EL::StatusCode::SUCCESS;
  }


	void HcgamgamTool::ClearTrue()
	{

		//Ints
		//truth
		m_eventInfoTrueInts["Hc_N_bjets"] = 0;
    	m_eventInfoTrueInts["Hc_N_cjets"] = 0;

    }

  void HcgamgamTool::Clear()
  {

		//clear vectors for each events and setting to some default value;
		//Ints
		m_eventInfoInts["Hc_jet1_truthlabel"] = -99;
		m_eventInfoInts["Hc_jet2_truthlabel"] = -99;


		//m_eventInfoChars["Hc_Atleast1jisloose"] = false;
		//m_eventInfoChars["Hc_Atleast1jistight"] = false;

		m_eventInfoInts["Hc_Atleast1jisloose"] = false;
		m_eventInfoInts["Hc_Atleast1jistight"] = false;

		m_eventInfoInts["Hc_Atleast1jisloose_index"] = 0;
		m_eventInfoInts["Hc_Atleast1jistight_index"] = 0;

		//Int
		m_eventInfoVInts["Hc_jet_truthlabel"].clear();
		m_eventInfoVInts["Hc_jetloose_tagged"].clear();
		m_eventInfoVInts["Hc_jettight_tagged"].clear();
		//Floats
		m_eventInfoVFloats["Hc_y1_j_dr"].clear();
		m_eventInfoVFloats["Hc_y2_j_dr"].clear();
		m_eventInfoVFloats["Hc_j_pt"].clear();
		m_eventInfoVFloats["Hc_j_eta"].clear();



		//Set the values
		//Ints
    	//reco
    	m_eventInfoInts["Hc_Njets"]   = 0;
    	m_eventInfoInts["Hc_m_yy_cat"] = -1;
    	m_eventInfoInts["Hc_leadJet_truthLabel"] = -99.;
    	m_eventInfoInts["Hc_StrangeEvents"]  = 0;
    	m_eventInfoInts["Hc_Leadisloose"] = 0;
		m_eventInfoInts["Hc_Leadistight"] = 0;

    	//Floats
		m_eventInfoFloats["Hc_h_y"]   = -99.;
    	m_eventInfoFloats["Hc_h_eta"] = -99.;
    	m_eventInfoFloats["Hc_h_phi"] = -99.;
    	m_eventInfoFloats["Hc_h_pt"]  = -1;
       	m_eventInfoFloats["Hc_leadJet_pt"]  = -1;
		m_eventInfoFloats["Hc_leadJet_eta"] = -99.;
		m_eventInfoFloats["Hc_leadJet_phi"] = -99.;

		m_eventInfoFloats["Hc_y1leadJet_dr"] = -1;
    	m_eventInfoFloats["Hc_y2leadJet_dr"] = -1;
    	m_eventInfoFloats["Hc_y1leadJet_dphi"] = -99.;
    	m_eventInfoFloats["Hc_y2leadJet_dphi"] = -99.;
    	m_eventInfoFloats["Hc_y1y2_dr"] = -1;
    	m_eventInfoFloats["Hc_y1y2_deta"] = -1;
    	m_eventInfoFloats["Hc_y1y2_dphi"] = -99.;
		m_eventInfoFloats["Hc_yyc_m"]   = -1;
		m_eventInfoFloats["Hc_yyc_pt"]  = -1;
		m_eventInfoFloats["Hc_yyc_y"]   = -99.;

		//m_eventInfoFloats["Hc_leadJet_pb"]  = -99999.;
		//m_eventInfoFloats["Hc_leadJet_pc"]  = -99999.;
		//m_eventInfoFloats["Hc_leadJet_pu"]  = -99999.;



	}
  // _______________________________________________________________________________________
  void HcgamgamTool::saveHCTruthInfo(const xAOD::TruthParticleContainer &photons,
                                     const xAOD::JetContainer   &jets,
                                     const xAOD::JetContainer   &bjets,
                                     const xAOD::JetContainer   &cjets,
                                     xAOD::TruthParticleContainer &muons,
                                     xAOD::TruthParticleContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
    //std::map<TString, float> trutheventInfoFloats;
    //std::map<TString, int>   trutheventInfoInts;
	ClearTrue();

    // Perform truth selection
    performTruthSelection(photons, jets, bjets, cjets, muons, electrons);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToTruthEventInfo();
  }



   // _______________________________________________________________________________________
  // Function which performs full HiggsHF truth selection and saves info to info maps (nominal by default)
  void HcgamgamTool::performTruthSelection(const xAOD::TruthParticleContainer &photons,
                                              const xAOD::JetContainer   &jets,
                                              const xAOD::JetContainer   &bjets,
                                              const xAOD::JetContainer   &cjets,
                                              xAOD::TruthParticleContainer &muons,
                                              xAOD::TruthParticleContainer &electrons)
  {

    // First apply photon selection
    if (!truthHandler()->passFiducial(&photons)) {
      m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCPHOTONS;
      return;
    }


    // Number of central jets
    int Njet_central = 0;
    int Njet_b = 0;
    int Njet_c = 0;


    //Loop over jets
    for (auto jet : jets) {
      if (fabs(jet->eta()) > m_truthjet_rapidity_max || jet->pt() < m_jet_pt * HG::GeV) { continue; }
      Njet_central++;
    }

    //Loop over bjets
    for (auto bjet : bjets) {
      if (fabs(bjet->eta()) > m_truthjet_rapidity_max || bjet->pt() < m_jet_pt * HG::GeV) { continue; }
      Njet_b++;
    }

    //Loop over cjets, reject jets that are already matched to a b-jet
    for (auto cjet : cjets) {
      if (fabs(cjet->eta()) > m_truthjet_rapidity_max || cjet->pt() < m_jet_pt * HG::GeV) { continue; }
      Njet_c++;
    }




    // Minimum requirement on the number of truth jets
    if (Njet_central == 0) { m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCATLEAST1JET; return;}

    // Require at least one jet matched to a c-hadron
    if (Njet_c == 0) { m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCCTAG; return;}

    // Save events that pass all requirements
    m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCPASS;

    m_eventInfoTrueInts["Hc_N_bjets"] = Njet_b;
    m_eventInfoTrueInts["HC_N_cjets"] = Njet_c;


  }


  // _______________________________________________________________________________________
  void HcgamgamTool::saveHCInfo(xAOD::PhotonContainer   photons,
                                xAOD::JetContainer  &jets,
                                xAOD::JetContainer  &jetsJVT,
                                xAOD::MuonContainer &muons,
                                xAOD::ElectronContainer &electrons)
 {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)
   // std::map<TString, float> eventInfoFloats;
    //std::map<TString, int>   eventInfoInts;



	Clear();
    // Perform selection
    performSelection(photons, jets, jetsJVT, muons, electrons);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToEventInfo();
 }

  // _______________________________________________________________________________________
  // Function which performs full h+c selection and saves info to info maps (nominal by default)
  void HcgamgamTool::performSelection(xAOD::PhotonContainer photons,
                                      xAOD::JetContainer &jets,
                                      xAOD::JetContainer &jets_noJVT,
                                      xAOD::MuonContainer &muons,
                                      xAOD::ElectronContainer &electrons)
  {


    // First apply photon selection
    if (photons.size() < 2) {
      m_eventInfoInts["Hc_cutFlowLeadJet"] = HCPHOTONS;
      m_eventInfoInts["Hc_cutFlowAllJet"]  = HCPHOTONS;
      return;
    }



	//----------------- Selecting the jets -----------------//

	//Disregard events with no jets
	if(jets.size() < 1 ){
		m_eventInfoInts["Hc_cutFlowLeadJet"] = HCATLEAST1JET;
		m_eventInfoInts["Hc_cutFlowAllJet"]  = HCATLEAST1JET;
      	return;
    }

   std::vector<int> centraljets;
   int counts = -1; //-1 no jets are found


    for(auto jet: jets){
    	counts++;
    	if (fabs(jet->eta()) > m_jet_eta_max || jet->pt() < m_jet_pt * HG::GeV) { continue; }
    	    centraljets.push_back(counts);
    	    if(centraljets.size()==m_Njets) break;
    }


	//Storing some other variables useful for the studies
	int Ncjet_tight = 0;
	int Ncjet_loose = 0;
    int Njets = 0;
    // Store info for the c-tag SF
    //no c-tagging SF is available set it to 1;
    double weight_c_tagging = 1.0;


     //VCheck how many jets are ctagged if they are good jets
    for (auto jet : jets) {
      if (isGoodJet(jet)) {
        Njets++;

        bool isCtight = isCTagTight(*jet);
        bool isCloose = isCTagLoose(*jet);

        if (isCtight) { Ncjet_tight++; }
        if (isCloose) { Ncjet_loose++; }

      }
    }


    m_eventInfoInts["Hc_Njets"]   	  = Njets;
    m_eventInfoInts["Hc_NcjetTight"]  = Ncjet_tight;
    m_eventInfoInts["Hc_NcjetLoose"]  = Ncjet_loose;



	//Define Higgs ------
	//-------------------------------
	TLorentzVector pho1 = (photons)[0]->p4();
	TLorentzVector pho2 = (photons)[1]->p4();


    //Perform the selection for the Leading jet only
    performleadjetCutflow(pho1,pho2,jets,centraljets);

    //Perform the selection for the jets min=1 max=3
    performjetCutflow(pho1,pho2,jets,centraljets);


	//Save some common variables
	TLorentzVector Vyy = pho1+pho2;
	m_eventInfoFloats["Hc_h_y"]   = Vyy.Rapidity();
    m_eventInfoFloats["Hc_h_eta"] = Vyy.PseudoRapidity();
    m_eventInfoFloats["Hc_h_phi"] = Vyy.Phi();
    m_eventInfoFloats["Hc_h_pt"]  = Vyy.Pt();


    //Sunt in case include the categories of the m_yy

    if (Vyy.M() >= 122.5 * HG::GeV && Vyy.M() <= 127.5 * HG::GeV) {
      m_eventInfoInts["Hc_m_yy_cat"] = SR;
    } else if (Vyy.M() <= 120 * HG::GeV || Vyy.M() >= 130 * HG::GeV) {
      m_eventInfoInts["Hc_m_yy_cat"] = SIDEBANDS;
    } else {
      m_eventInfoInts["Hc_m_yy_cat"] = VR;
    }




    //Checking the weights
    // Calculate the JVT scale-factor from uncorrected, - 30 GeV for now
    double weight_JVT = weightJVT(jets_noJVT);

    // Get the weight from the JVT + from the ctagging
    m_eventInfoFloats["Hc_weight"] = weight_JVT * weight_c_tagging;



  }//end of performSelection




  void HcgamgamTool::performleadjetCutflow(TLorentzVector ph1,
  										   TLorentzVector ph2,
  										   xAOD::JetContainer &jets,
  										   std::vector<int> centjets)
  {


	//Define ints, bools, floats
    bool pass_tight = 0;
    bool pass_loose = 0;


	//Require to have at least one central jet
    if (centjets.size() <= 0) { m_eventInfoInts["Hc_cutFlowLeadJet"] = HCJETPTETA; return; }

	//Use the leading jet to perform the measurements
    if (centjets.at(0) > 0) { m_eventInfoInts["Hc_cutFlowLeadJet"] = HCLEADJETPTETA; return; }

    //Lead jet
    const xAOD::Jet *leadjet = (jets)[0];
    TLorentzVector jet  = leadjet->p4();


    //perform the dR on the leading jet - tight dR cut
	if( (jet.DeltaR(ph1) < m_dRcut) || (jet.DeltaR(ph2) < m_dRcut) )
		{m_eventInfoInts["Hc_cutFlowLeadJet"] = DRJETGAM; return;}


	//C-tagging - this c-tagging is the old one, no new c-tagging is available
	bool isCtag_loose = isCTagLoose(*leadjet);
	bool isCtag_tight = isCTagTight(*leadjet);


	//Save the events that passed all requirements
	m_eventInfoInts["Hc_cutFlowLeadJet"] = HCPASS;


	//You dont want to use loose ctagged jets? Use LeadisTight branch!
	if(isCtag_tight == true)
		{pass_tight = true;}

	if(isCtag_loose == true)
		{pass_loose = true;}

	int strange = 0;
	//The is one event in H+c sample where the leading jet tagged tight, doesn't pass loose
	if(pass_tight == true and pass_loose == false) {strange++;}



	//Vars to store
	//Floats
	m_eventInfoFloats["Hc_leadJet_pt"]  = jet.Pt();
	m_eventInfoFloats["Hc_leadJet_eta"] = jet.Eta();
	m_eventInfoFloats["Hc_leadJet_phi"] = jet.Phi();

	m_eventInfoFloats["Hc_y1leadJet_dr"] = ph1.DeltaR(jet);
    m_eventInfoFloats["Hc_y2leadJet_dr"] = ph2.DeltaR(jet);
    m_eventInfoFloats["Hc_y1leadJet_dphi"] = ph1.DeltaPhi(jet);
    m_eventInfoFloats["Hc_y2leadJet_dphi"] = ph2.DeltaPhi(jet);
    m_eventInfoFloats["Hc_y1y2_dr"] = ph1.DeltaR(ph2);
    m_eventInfoFloats["Hc_y1y2_deta"] = ph1.Eta()-ph2.Eta();
    m_eventInfoFloats["Hc_y1y2_dphi"] = ph1.DeltaPhi(ph2);

    TLorentzVector Vecyy = ph1+ph2;

	TLorentzVector Hc = Vecyy + jet;
	m_eventInfoFloats["Hc_yyc_m"]   = Hc.M();
	m_eventInfoFloats["Hc_yyc_pt"]  = Hc.Pt();
	m_eventInfoFloats["Hc_yyc_y"]   = Hc.Rapidity();



    //Truth info
    int truthLabel = HG::isMC() ? leadjet->auxdata<int>("HadronConeExclTruthLabelID") : -99;

    //Ints
	m_eventInfoInts["Hc_leadJet_truthLabel"] = truthLabel;
	m_eventInfoInts["Hc_Leadisloose"] = pass_loose;
	m_eventInfoInts["Hc_Leadistight"] = pass_tight;
	m_eventInfoInts["Hc_StrangeEvents"] = strange;

  }


	void HcgamgamTool::performjetCutflow(TLorentzVector ph1,
										 TLorentzVector ph2,
										 xAOD::JetContainer &jets,
  										 std::vector<int> centjets)
    {

    	int  isLoose_index = -1, isTight_index = -1;//index of tagged jets -1 no jets are found
    	bool isLoose = false, isTight = false;//the flag to select the tagged jets
        int isPassdR;
        bool ispassed1 = false;
        TLorentzVector jet;
        int truthLabel1;
        int truthLabel2;
        int truthLabel;

        //Require to have at least one jet in the container
        if (centjets.size() <= 0) {m_eventInfoInts["Hc_cutFlowAllJet"] = HCJETPTETA; return;}


        for(int i = 0; i< centjets.size(); i++){
        	//Check dR
        	isPassdR = -1;
        	jet  = jets.at(centjets.at(i))->p4();
        	float dR_y1_j = jet.DeltaR(ph1);
        	float dR_y2_j = jet.DeltaR(ph2);

        	if( (jet.DeltaR(ph1) > m_dRcut) && (jet.DeltaR(ph2) > m_dRcut) ) {isPassdR = 1; ispassed1 = true;}

			//if(isPassdR==1 && i==0){truthLabel1 = HG::isMC() ? jets.at(i)->auxdata<int>("HadronConeExclTruthLabelID") : -99;}
			//if(isPassdR==1 && i==1){truthLabel2 = HG::isMC() ? jets.at(i)->auxdata<int>("HadronConeExclTruthLabelID") : -99;}

			if(isPassdR==1){
				truthLabel = HG::isMC() ? jets.at(i)->auxdata<int>("HadronConeExclTruthLabelID") : -99;
				m_eventInfoVInts["Hc_jet_truthlabel"].push_back(truthLabel);
	        	m_eventInfoVFloats["Hc_y1_j_dr"].push_back(dR_y1_j);
				m_eventInfoVFloats["Hc_y2_j_dr"].push_back(dR_y2_j);
				m_eventInfoVFloats["Hc_j_pt"].push_back(jet.Pt());
				m_eventInfoVFloats["Hc_j_eta"].push_back(jet.Eta());
			}

        	//Check c-tagging
        	if (isCTagLoose(*jets.at(centjets.at(i))) && isPassdR == 1 && isLoose_index == -1 ) {
        		isLoose_index = centjets.at(i); isLoose = true;
        	}


	        if (isCTagTight(*jets.at(centjets.at(i))) && isPassdR == 1 && isTight_index == -1 ) {
	            isTight_index = centjets.at(i); isTight = true;
	        }

			if(isPassdR==1 && isLoose==1){m_eventInfoVInts["Hc_jetloose_tagged"].push_back(isLoose);}
			if(isPassdR==1 && isTight==1){m_eventInfoVInts["Hc_jettight_tagged"].push_back(isTight);}


        }//end of for loop

		//eventInfoInts["Hc_jet1_truthlabel"]   = truthLabel1;
		//eventInfoInts["Hc_jet2_truthlabel"]   = truthLabel2;

		if (ispassed1==false) {m_eventInfoInts["Hc_cutFlowAllJet"] = DRJETGAM; return;}

		//Save the events that passed all requirements
		m_eventInfoInts["Hc_cutFlowAllJet"] = HCPASS;


		//m_eventInfoChars["Hc_Atleast1jisloose"] = isLoose;
		//m_eventInfoChars["Hc_Atleast1jistight"] = isTight;

		m_eventInfoInts["Hc_Atleast1jisloose"] = isLoose;
		m_eventInfoInts["Hc_Atleast1jistight"] = isTight;

		m_eventInfoInts["Hc_Atleast1jisloose_index"] = isLoose_index;
		m_eventInfoInts["Hc_Atleast1jistight_index"] = isTight_index;


    }




  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  double HcgamgamTool::weightJVT(xAOD::JetContainer &jets_noJVT)
  {

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > m_jet_eta_max) { continue; }

      if (jet->pt() < m_jet_pt * HG::GeV) { continue; }

      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }



  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HcgamgamTool::saveMapsToEventInfo()
  {
     // Floats
    for (auto element : m_eventInfoFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : m_eventInfoInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }

    //Vectors of Ints
    for (auto element : m_eventInfoVInts){
    	eventHandler()->storeVar<std::vector<int>>(element.first.Data(), element.second);
    }

	//Vectors of floats
    for (auto element : m_eventInfoVFloats){
    	eventHandler()->storeVar<std::vector<float>>(element.first.Data(), element.second);
    }

    //Vectors of floats
    for (auto element : m_eventInfoChars){
    	eventHandler()->storeVar<char>(element.first.Data(), element.second);
    }
  }

   // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HcgamgamTool::saveMapsToTruthEventInfo()
  {
    // Floats
    //for (auto element : eventInfoFloats) {
    // eventHandler()->storeTruthVar<float>(element.first.Data(), element.second);
    //}

    // Ints
    for (auto element : m_eventInfoTrueInts) {
      eventHandler()->storeTruthVar<int>(element.first.Data(), element.second);
    }
  }


  //--------------------------------------------------------------------------------------------------
  //Discriminant function//very easy
  double HcgamgamTool::discriminant(double pc, double pb, double pu, float fraction)
  {
    double ctag_discriminant = log( pc / ( (pb * fraction) + (1 - fraction)*pu ) );
    return ctag_discriminant;
  }





  bool HcgamgamTool::isCTagLoose(const xAOD::Jet &jet)
   {
   	   static SG::AuxElement::Accessor<char> isDL1CtagLoose("DL1_CTag_Loose");


       if (isDL1CtagLoose.isAvailable(jet)) { return isDL1CtagLoose(jet); }
       else {
        if (jet.btagging()) {
          static const float fb = 0.18;
          static const double dl1_cut = 0.61456;
          double pb = -99;
             jet.btagging()->pb(m_tagger.Data(), pb);
          double pc = -99;
             jet.btagging()->pc(m_tagger.Data(), pc);
          double pu = -99;
             jet.btagging()->pu(m_tagger.Data(), pu);

          if (pb > -99. && pu > -99. && pc > -99.) {
            double dl1 = discriminant(pc,pb,pu,fb);
            return dl1 > dl1_cut;
          }
          else { return false; }

        } else { return false; }
      }

   }


   bool HcgamgamTool::isCTagTight(const xAOD::Jet &jet)
   {
   	   static SG::AuxElement::Accessor<char> isDL1CtagTight("DL1_CTag_Tight");


       if (isDL1CtagTight.isAvailable(jet)) { return isDL1CtagTight(jet); }
       else {
        if (jet.btagging()) {
          static const float fb = 0.08;
          static const double dl1_cut = 1.3;
          double pb = -99;
             jet.btagging()->pb(m_tagger.Data(), pb);
          double pc = -99;
             jet.btagging()->pc(m_tagger.Data(), pc);
          double pu = -99;
             jet.btagging()->pu(m_tagger.Data(), pu);

          if (pb > -99. && pu > -99. && pc > -99.) {
            double dl1 = discriminant(pc,pb,pu,fb);
            return dl1 > dl1_cut;
          }
          else { return false; }

        } else { return false; }
      }

   }

   bool HcgamgamTool::isGoodJet(const xAOD::Jet *jet)
  {
    if ( jet->pt() < m_jet_pt * HG::GeV && fabs(jet->eta()) > m_jet_eta_max) { return false; }
	else
    	return true;
  }

  // _______________________________________________________________________________________
  // Fetch the ctagging SF weight
  double HcgamgamTool::weightCTagging(const xAOD::Jet *jet)
  {

    TString SFName = TString("SF_DL1_CTag_Loose");
    return jet->isAvailable<float>(SFName.Data()) ? jet->auxdata<float>(SFName.Data()) : -1;
  }



}//end of the hctool




/*
	//Make the lepton veto (do we need this?)
    xAOD::MuonContainer muonsPassing(SG::VIEW_ELEMENTS);
    xAOD::ElectronContainer electronsPassing(SG::VIEW_ELEMENTS);

    // Apply muon requirements
    std::copy_if(muons.begin(), muons.end(), std::back_inserter(muonsPassing), [this](const xAOD::Muon * muon) {
      return (muon->pt() >= m_minMuonPT * HG::GeV);
    });


    // Apply electron requirements
    std::copy_if(electrons.begin(), electrons.end(), std::back_inserter(electronsPassing), [this](const xAOD::Electron * electron) {
      return (electron->pt() >= m_minElectronPT * HG::GeV);
    });


    // Apply lepton veto
    if (muonsPassing.size() > 0) { eventInfoInts["Hc_cutFlow"] = HCLEPVETO; return;}
    if (electronsPassing.size() > 0) { eventInfoInts["Hc_cutFlow"] = HCLEPVETO; return;}

*/

