#include "hcgamgamanalysis/hcskim.h"

#include "HGamAnalysisFramework/HGamVariables.h"
// this is needed to distribute the algorithm to the workers
ClassImp(SkimAndSlim)



hcskim::hcskim(const char *name)
  : SkimAndSlim(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode hcskim::createOutput()
{
  // Check whether unfolding skimming should be applied
  m_unfoldSkim = config()->getBool("hcskim.UnfoldingSkim", true);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode hcskim::execute()
{
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  //============================================================================
  // Here you do everything that needs to be done on all events, e.g. fill
  // efficiency histograms. You can only retrieve const containers and info
  // objects at this point.

  //============================================================================
  // Check if the event passes possible skimming configuration. You can add
  // your own event skimming here, though you can't update object decorations to
  // make your decision. You can only retrieve const containers and info objects
  // at this point.
  bool skip = false;
  ANA_CHECK(SkimAndSlim::skipEvent(skip));

  if (skip) { return EL::StatusCode::SUCCESS; }

  // XSec defined function checking if the event (isPassed || isFiducial)
  // If it's data, the cutFlow flag is used (skip this)
  if (m_unfoldSkim && HG::isMC()) {
    ANA_CHECK(skipXSecEvent(skip));

    if (skip) { return EL::StatusCode::SUCCESS; }
  }



  //============================================================================
  // Record configured containers to output. After calling recordContainer()
  // you are able to retrieve non-const containers and info objects you're
  // saving to the output. After this function call, you can
  // do everything that needs to be done on events passing the skimming,
  // e.g. add additional variables, object decorations.
  // After calling recordContainers() you always need to call fillEvent()
  ANA_CHECK(SkimAndSlim::recordContainers());

  // Add a few variables
  //static SG::AuxElement::ConstAccessor<int> N_j_30("N_j_30");
  //static SG::AuxElement::ConstAccessor<float> pT_yy("pT_yy");
  static SG::AuxElement::ConstAccessor<float> weight("weight");
  static SG::AuxElement::ConstAccessor<float> weightJvt_30("weightJvt_30");
  static SG::AuxElement::ConstAccessor<float> Hc_weight("Hc_weight");


  static SG::AuxElement::ConstAccessor<char> isPassed("isPassed");
  //static SG::AuxElement::ConstAccessor<char> isFiducial("isFiducial");

  xAOD::EventInfo *eventInfo = nullptr;


  static SG::AuxElement::ConstAccessor<int> Hc_Atleast1jisloose("Hc_Atleast1jisloose");
  static SG::AuxElement::ConstAccessor<int> Hc_Atleast1jistight("Hc_Atleast1jistight");
  static SG::AuxElement::ConstAccessor<int> Hc_Leadisloose("Hc_Leadisloose");
  static SG::AuxElement::ConstAccessor<int> Hc_Leadistight("Hc_Leadistight");

  static SG::AuxElement::ConstAccessor<int> Hc_cutFlowAllJet("Hc_cutFlowAllJet");
  static SG::AuxElement::ConstAccessor<int> Hc_cutFlowLeadJet("Hc_cutFlowLeadJet");


  static SG::AuxElement::ConstAccessor<float> m_yy("m_yy");



  // Update nominal and systematic detector-level
  std::vector<std::string> syslist = getSystematicNames();
  syslist.push_back("");

  for (std::string sys : syslist) {
    std::string cont = "HGamEventInfo" + sys;
    eventInfo = nullptr;
    ANA_CHECK(getContainer<xAOD::EventInfo>(eventInfo, cont));



    // if (sys == "" && wk()->xaodEvent()->contains<xAOD::PhotonContainer>("HGamPhotons")) {
    //   static SG::AuxElement::ConstAccessor<char> isPassedRelPtCuts("isPassedRelPtCuts");
    //   static SG::AuxElement::ConstAccessor<char> isPassedIsolation("isPassedIsolation");
    //   static SG::AuxElement::ConstAccessor<char> isTight("isTight");
    //   static SG::AuxElement::ConstAccessor<char> isIso("isIsoFixedCutLoose");
    //   static SG::AuxElement::ConstAccessor<unsigned int> isEMTight("isEMTight");

    //   static SG::AuxElement::Accessor<char> catXS_ttH_loose("catXS_ttH_loose");
    //   static SG::AuxElement::Accessor<char> isPassedYJ("isPassedYJ");
    //   static SG::AuxElement::Accessor<char> isPassedJY("isPassedJY");
    //   static SG::AuxElement::Accessor<char> isPassedJJ("isPassedJJ");

    //   const xAOD::PhotonContainer *photons = nullptr;
    //   ANA_CHECK( getConstContainer<xAOD::PhotonContainer>(photons, "HGamPhotons") );
    //   const xAOD::Photon *lead = (*photons)[0];
    //   const xAOD::Photon *subl = (*photons)[1];

    //   bool lead_looseNotTight = !isTight(*lead) || !isIso(*lead);
    //   bool subl_looseNotTight = !isTight(*subl) || !isIso(*subl);

    //   bool lead_looseP4 = !(isEMTight(*lead) & HG::PhotonLoosePrime4) && !isTight(*lead);
    //   bool subl_looseP4 = !(isEMTight(*subl) & HG::PhotonLoosePrime4) && !isTight(*subl);

    //   bool ttH_semi = N_lep_15(*eventInfo) >= 1 && N_j_30(*eventInfo) >=3;
    //   bool ttH_had  = N_lep_15(*eventInfo) == 0 && N_j_30(*eventInfo) >=4;
    //   catXS_ttH_loose(*eventInfo) = isPassedRelPtCuts(*eventInfo) && (lead_looseNotTight || subl_looseNotTight) && (ttH_semi || ttH_had);

    //   bool basicLoose = isPassedRelPtCuts(*eventInfo) && isPassedIsolation(*eventInfo);
    //   isPassedYJ(*eventInfo) = basicLoose && isTight(*lead) && subl_looseP4;
    //   isPassedJY(*eventInfo) = basicLoose && lead_looseP4 && isTight(*subl);
    //   isPassedJJ(*eventInfo) = basicLoose && lead_looseP4 && subl_looseP4;
    // }
  }

  std::vector<std::string> contToSave = getContainersToSave();

  if (HG::isMC() && std::find(contToSave.begin(), contToSave.end(), "HGamTruthEventInfo") != contToSave.end()) {
    // Update particle-level
    eventInfo = nullptr;
    ANA_CHECK(getContainer<xAOD::EventInfo>(eventInfo, "HGamTruthEventInfo"));

  }



  //============================================================================
  // Fill output file with event. This writes the event to disk, so should be
  // the last thing that happens.
  SkimAndSlim::fillEvent();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode hcskim::skipXSecEvent(bool &skip)
{
  // Check if the reco event passes selection for any systematic variation, or
  // if the truth event passes selection. Otherwise skip.
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  // Default behavior is to keep event
  skip = false;

  const xAOD::EventInfo *cEventInfo = nullptr;
  static SG::AuxElement::ConstAccessor<char> isPassedBasic("isPassedBasic");
  static SG::AuxElement::ConstAccessor<char> isPassed("isPassed");
  static SG::AuxElement::ConstAccessor<char> isFiducial("isFiducial");

  // Check nominal true pass
  ANA_CHECK(getConstContainer<xAOD::EventInfo>(cEventInfo, "HGamTruthEventInfo"));

  if (isFiducial.isAvailable(*cEventInfo) && isFiducial(*cEventInfo))
  { return EL::StatusCode::SUCCESS; }

  // Check nominal and systematic reco pass
  std::vector<std::string> syslist = getSystematicNames();
  syslist.push_back("");

  for (std::string sys : syslist) {
    std::string cont = "HGamEventInfo" + sys;
    ANA_CHECK(getConstContainer<xAOD::EventInfo>(cEventInfo, cont));

    if (isPassedBasic(*cEventInfo) && isPassed(*cEventInfo))
    { return EL::StatusCode::SUCCESS; }
  }

  skip = true;
  return EL::StatusCode::SUCCESS;
}
