This package is for H+c analysis, where we are searching for Higgs decaying to di-photons, and one c jet
This package will make the MxAOD files to be used later here: https://gitlab.cern.ch/aivina/NTupAnalyser


## Quik start:

**To clone the git repository**

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/aivina/hcgamgamanalysis.git 

Option --recursive will download also the subpackages which your code depends on 
```

 **Get the latest changes**:

```
git pull --all

```

**To check what you have done after you made changes**:
```
git status
```

**To add changes**:

```
git add filename.txt

```

**Commit the changes** :
```
git commit -m "a short message here"
```

**Send the changes to the brunch**:
```
git push -u origin master
```
```
Now your changes in the git repository and you need to merge them with the master branch:

git checkout master
git merge my-local-branch
git push origin master
```

##
## Updating the HGamCore Tag

If you are a developer trying to update to a new HGamCore tag, do e.g.:

```
cd hcgamgamanalysis/HGamCore
git checkout v1.3.0-h019
git submodule update --init --recursive

don't forget to commit to the hcgamgamanalysis repository.
```

**The versions of HGamTool**


| HGamCore tag | HGamCore commit hash | Analysis Release |
| --- | --- | -- |
| v1.5.5-h021 | 4ea5005a | asetup AnalysisBase,21.2.40 |
| v1.8.1-h024 | 9f868a8b | asetup AnalysisBase,21.2.56 |
| v1.8.13-h024| 6cf53733 | asetup AnalysisBase,21.2.56 |
| v1.9.7-h025-fixPSCBTag |          | asetup AnalysisBase,21.2.113 |



h021 vs. h024: https://indico.cern.ch/event/778097/contributions/3241634/attachments/1765432/2866064/HGam-ReleaseStatus_h024.pdf
(the h tag shall be updated to the latest)

**Versions of asetup**

asetup AnalysisBase,21.2.40 (for v021) and 21.2.56 (for v024)

If you want to use the newest version of asetup, you need completely empty the build directory
##

## How to run the code

1. In the source directory setup: asetup AnalysisBase,21.2.56,here  (for the Emtopo)
2. For the PFlow we have changed release to asetup AnalysisBase,21.2.113,here for the h026 analysis AnalysisBase,21.2.131,here
2. Go to the build directory and do: cmake ../source  and then cmake --build .
	
After that, don't forget to do : source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh

In the Folder ListFiles please creat the list of the files you want to run on locally.

runHcgamAnalysis hcgamgamanalysis/HcgamAnalysis.config InputFileList: ListFiles/myy_90_175.list NumEvents: 100


**Useful pdfs with the inherited code description**
https://indico.cern.ch/event/438856/contributions/1940391/attachments/1141382/1635882/xAODCrew_20150818_HGamPlenary_MxAODTutorial.pdf
https://indico.cern.ch/event/438856/contributions/1940392/attachments/1141340/1634968/JVasquez_HGamma_Aug18.pdf


**Running the code on the GRID:**

lsetup panda && voms-proxy-init -voms atlas --valid 96:00

runHcgamAnalysis hcgamgamanalysis/HcgamAnalysis.config GridDS: YOUR FAVORITE SAMPLE OutputDS: YOUR OUTPUT 

##

## What the code is doing 

There are several steps performed in the code

**I. Declaration - declareOutputVariables()**

Will help to store the variables which are relevant for Data or MC

**II. createOutput()**

Determines what to save in the output root files

Also takes the cuts from the analysis config file such as: SkimmingCut, minMuonPT, minElectronPT

So far we have: Photons, Jets, EventInfo, TruthEvetns, Muons, Electrons, TruthJets,TruthElectrons,TruthMuons,TruthPhotons

(PFlow reco jets are there, but we shall not use them yet)


**III. execute()**

There are several functions and features to put attention at:

1. **Cutflow** - makes the cutflow histograms - Normal cut flow, Weighted cutflow.., with and without Dalitz Events

   **Features:**
   
   * _Cuts on events:_ CUT_0: DUBLICATE events, CUT_1: GRL, CUT_2: TRIGGER, CUT_3: DQ (LAr,Tile,SCT), CUT_4: Vertex
   
   * _Cuts on photons:_ CUT_5: 2 loose photons (passed 0Q,Cleaning,PtetaCut,LooseID), CUT_6: Photons passed Ambiguity,HV cut - in the inherited code those photons are called Preselected, CUT_7: Photons Matched to trigger, CUT_8: Passed Trigger match, CUT_9: Photons isolated, CUT_10: Passed Relative pT cut
   
   * _Mass cut:_ CUT_11: Mass Cut [105,160] GeV
   
   * _Cuts on jets:_ CUT_12: Require at least 1 jet,CUT_13: Require at least one jet to have |eta|<2.5
   
   After each cut, the events passed the certain requirements are filled into the histogram
   
   **important:** This function doesnt perform any "true" event cuts 


2. cut  if (m_skimCut >= 1 && m_cutFlow <= m_skimCut) { return EL::StatusCode::SUCCESS; } --> events are saved only after sertain cut provided in the config. 
 
   The preference so far: Save events where 2 photons passed the LooseID - **Skimmingcut = 9** 
   

3. **doReco()** - this is the main function, which selects photons, jets, writes the outputs 
   
   Photons: For the analysis we will save the Preselected photons - good photons, NOT isolated, with LooseID
   
   Jets: We will save the jets that are good jets passed all the selection requirements (applySelection)
   
   Muons and Electrons: We will select good muons and electrons in order to use them in the overlapremoval() function
  
   _OverlapHandler()_ - What ever we do - Photons first. The function removes events according to the following scheme:
   * The two leading photons are always kept;
   * Electrons with DeltaR(e,&gamma;) < 0.4 are removed;
   * Jets such as DeltaR(jet,e) < 0.2 or DeltaR(jet,gamma;) < 0.4 are removed
   * Muons with DeltaR(&mu;,jet) < 0.4 or DeltaR(&mu;,gamma;) < 0.4 are removed
   
   Lepton veto cut - so far not used

   _writeNominalOnly()_  and _writeDetailed()_ : 
    * functions which write the variables but only the niminal ones. (Systematics not yet implemented!!)
                           
    * Saves additional variables which can be used later, also saves the flags (false, true):  trigger matched ph., Isolated ph, pass TightID, pass Relative pt cut, mass Mass cut
                           
    * Saves the additional flags on the jets - isGoodJet (eta cut), At least 1 jet
    
    * Saves also the jet DL1 pb,pu,pc for the jet container 
                           
   
## Information about the Dalitz events
The Dalitz events present in data. Also they present in the Pythia8 decayed samples. 

We shall not remove them from the cutflow histogram, but we need to account for them while normolizing the samples.

The detailed stduyies were performed here: https://hgamdocs.web.cern.ch/Weights.html?highlight=jvt

It will say what weights shall be included, how they shall be included and how to treat the Weighted sum of weights.

The table below shows the Sum of weights for each background and the signal sample:

| MC sample | Sum of Weights | Sum of Weights no Dalitz |
| --- | --- | -- |
| ggF    | 2.19967E+08 | 2.06558E+08 |
| VBF    | 7.53039E+06 | 7.07327E+06 |
| W^{-}H | 135904.16   | 127614.08   |
| W^{+}H | 215989.70   | 202655.64   |
| ggZH   | 2819.6      | 2649.79     |
| qqZH   | 361508.25   | 339518.81   |
| bbH    | 64196.79    | 60272       |
| ttH    | 1042643.2   | 978990.44   |
|  γγ +jets (fulsim)| 8.83187E+06 | 8.83187E+06 |
| Hc     | 8.159     | 7.671    |
 
## The MC samples which are available for the analysis with the relevant tags
Sample names taken partically from from https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/20432/ and https://twiki.cern.ch/twiki/bin/view/Sandbox/RuggeroTurraSandbox#Zgamgam_background)

**the p tag is 3665 - seems like a good one**

 1.a ***gg->ZH (Cross section x BR --> 0.2782 pb)*** 
```
 mc16a: mc16_13TeV.345061.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc.deriv.DAOD_HIGG1D1.e5762_s3126_r9364_p3665
 mc16d: mc16_13TeV.345061.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc.deriv.DAOD_HIGG1D1.e5762_s3126_r10201_p3665
 mc16e: mc16_13TeV.345061.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc.deriv.DAOD_HIGG1D1.e5762_s3126_r10724_p3665
```

1.b ***qq->ZH (Cross section x BR --> 1.725 pb)***

```
mc16_a: mc16_13TeV.345319.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Hyy_Zincl_MINLO.deriv.DAOD_HIGG1D1.e5743_e5984_s3126_r9364_r9315_p3665
mc16_d: mc16_13TeV.345319.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Hyy_Zincl_MINLO.deriv.DAOD_HIGG1D1.e5743_s3126_r10201_p3665
mc16_e: mc16_13TeV.345319.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Hyy_Zincl_MINLO.deriv.DAOD_HIGG1D1.e5743_s3126_r10724_p3665
```


2.a ***pp->WminusH (Cross section x BR --> 1.206 pb)***

```
mc16_a: mc16_13TeV.345317.PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_e5984_s3126_r9364_r9315_p3665
mc16_d: mc16_13TeV.345317.PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_s3126_r10201_p3665
mc16_e: mc16_13TeV.345317.PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_s3126_r10724_p3665
```

2.b ***pp->WplusH (Cross section x BR --> 1.902 pb)***

```
mc16_a: mc16_13TeV.345318.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_e5984_s3126_r9364_r9315_p3665
mc16_d: mc16_13TeV.345318.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_s3126_r10201_p3665
mc16_e: mc16_13TeV.345318.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Hyy_Wincl_MINLO.deriv.DAOD_HIGG1D1.e5734_s3126_r10724_p3665
```


3. ***Z->ee*** 

```
mc16a: 
```


4. ***ggF  (Cross section x BR --> 110.1 pb)***

```
mc16_a: mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_s3126_r9364_r9315_p3665
mc16_d: mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_s3126_r10201_p3665
mc16_e: mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_s3126_r10724_p3665
```

5. ***VBF (Cross section x BR --> 8.578 pb)***

```
mc16_a: mc16_13TeV.345041.PowhegPythia8EvtGen_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam.deriv.DAOD_HIGG1D1.e5720_e5984_s3126_r9364_r9315_p3665
mc16_d: mc16_13TeV.346214.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam.deriv.DAOD_HIGG1D1.e6970_s3126_r10201_p3705
mc16_e: mc16_13TeV.346214.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam.deriv.DAOD_HIGG1D1.e6970_s3126_r10724_p3714
```

6. ***bbH (Cross section x BR --> 1.104 pb)*** 

```
mc16_a: mc16_13TeV.345315.PowhegPythia8EvtGen_A14NNPDF23LO_bbHyy.deriv.DAOD_HIGG1D1.e6050_s3126_r9364_p3665
mc16_d: mc16_13TeV.345315.PowhegPythia8EvtGen_A14NNPDF23LO_bbHyy.deriv.DAOD_HIGG1D1.e6050_s3126_r10201_p3665
mc16_e: mc16_13TeV.345315.PowhegPythia8EvtGen_A14NNPDF23LO_bbHyy.deriv.DAOD_HIGG1D1.e6050_s3126_r10724_p3665
```

7. ***gammagamma+several jets (Cross section x BR --> 51823 pb)***

```
mc16_a: mc16_13TeV.364352.Sherpa_224_NNPDF30NNLO_Diphoton_myy_90_175.deriv.DAOD_HIGG1D1.e6452_a875_r9364_p3663
mc16_d: mc16_13TeV.364352.Sherpa_224_NNPDF30NNLO_Diphoton_myy_90_175.deriv.DAOD_HIGG1D1.e6452_s3126_r10201_p3714
mc16_e: mc16_13TeV.364352.Sherpa_224_NNPDF30NNLO_Diphoton_myy_90_175.deriv.DAOD_HIGG1D1.e6452_s3126_r10724_p3714
```
8. ***ttH (Cross section x BR --> 1.150 pb)***

```
mc16_a: mc16_13TeV.345863.PowhegPythia8EvtGen_A14NNPDF23_NNPDF30ME_ttH125_gamgam.deriv.DAOD_HIGG1D1.e6503_s3126_r9364_p3665
mc16_d: mc16_13TeV.345863.PowhegPythia8EvtGen_A14NNPDF23_NNPDF30ME_ttH125_gamgam.deriv.DAOD_HIGG1D1.e6503_s3126_r10201_p3665
mc16_e: mc16_13TeV.345863.PowhegPythia8EvtGen_A14NNPDF23_NNPDF30ME_ttH125_gamgam.deriv.DAOD_HIGG1D1.e6503_s3126_r10724_p3665
```
***Signal samples (Cross section x BR --> 1.01923 pb)***
```
mc16_a: mc16_13TeV.346394.MadGraphPythia8EvtGen_A14NNPDF23_cH_Hgammagamma.deriv.DAOD_HIGG1D1.e7180_e5984_s3126_r9364_r9315_p3714
mc16_d: mc16_13TeV.346394.MadGraphPythia8EvtGen_A14NNPDF23_cH_Hgammagamma.deriv.DAOD_HIGG1D1.e7180_e5984_s3126_r10201_r10210_p3714
mc16_e: mc16_13TeV.346394.MadGraphPythia8EvtGen_A14NNPDF23_cH_Hgammagamma.deriv.DAOD_HIGG1D1.e7180_e5984_s3126_r10724_r10726_p3714
```
##
