#ifndef hcgamgamanalysis_HcgamAnalysis_H
#define hcgamgamanalysis_HcgamAnalysis_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "hcgamgamanalysis/HcgamgamTool.h"


/*!
   //The strategy for the overlap removing
   *
   *  Run 1 HGam strategy<ul>
   *  <li>The two leading photons are always kept, photons first;
   *  <li>Electrons with &Delta;R(e,&gamma;) < 0.4 are removed;
   *  <li>Jets such as &Delta;R(jet,e) < 0.2 or &Delta;R(jet,&gamma;) < 0.4 are removed
   *  <li>Muons with &Delta;R(&mu;,jet) < 0.4 or &Delta;R(&mu;,&gamma;) < 0.4 are removed
   *  </ul>
   *
   *  \author Kaicheng Li
   *  \author Dag Gillberg
   */


class HcgamAnalysis : public HgammaAnalysis
{

public:

private:

    HG::HcgamgamTool *m_hcgamgamTool; //!
    // Cut-flow - need to keep the same order!
    enum CutEnum {NxAOD = 0, NDxAOD = 1, ALLEVTS = 2, DUPLICATE = 3, GRL = 4, TRIGGER = 5, DQ = 6, VERTEX = 7, TWO_LOOSE_GAM = 8, AMBIGUITY = 9,
    TRIG_MATCH = 10, GAM_TIGHTID = 11, GAM_ISOLATION = 12, RELPTCUTS = 13, MASSCUT = 14, PASSALL = 15};


    enum yycCutFlowEnum {PHOTONS = 0, LEPVETO = 1, BTAG = 3, CTAG = 4};
    //enum btagWP {WP100=0, WP85 =1, WP77 =2, WP70 = 3, WP60 = 4}
    //enum ctagWP {} - Yet to be implemented

    // names of all cuts (do not includ "pass all")
    const std::vector<TString> s_cutDescs =
    {"#it{N}_{xAOD}","#it{N}_{DAOD}","All events","No dublicates","GRL","Pass trigger","Detector DQ","Has PV",
     "2 loose photons","e-#gamma ambiguity","Trigger match","tight ID","isolation","rel. #it{p}_{T} cuts","#it{m }_{#gamma#gamma} #in [105,160] GeV"};
  	/// value of cut that fail selection: PASSALL if all cuts passed
  	CutEnum m_cutFlow;
  	yycCutFlowEnum m_cutFlowyyc;

  	// names of the output containers
  	TString m_photonContainerName, m_jetContainerName, m_elecContainerName, m_muonContainerName;
  	TString m_photonTruthContainerName, m_jetTruthContainerName, m_elecTruthContainerName, m_muonTruthContainerName;
  	TString m_evtInfoName, m_truthEvtsName;


  	// what skimming to apply
  	int m_skimCut;

  	 // Algorithm setup
    TString m_BTagWP;           //! Which b-tagging WP should be used?



  	//Whether it is Dalitz event
  	bool m_isDalitz;
  	bool m_newFile;

    // store the crossSectionBRfilterEff value once per file
    float m_crossSectionBRfilterEff;

    // whether to apply systematics, save the differential variables and the truth
    bool m_applySystematics, m_saveObjects, m_saveDetailed, m_saveTruthObjects, m_saveTruthVars;
    bool m_allowMoreThanTwoPhotons;

    // whether to save fake photon combinations
    bool m_enableFakePhotons;
    //If we have two good fakes then we need to pass the slimming
    bool m_goodFakeComb;

    bool m_skimandslim;


    //Whether we are running yybb-tool in detailed mode
    bool m_detailedyyc;


    // cut-flow histograms and book keeper yields
  	std::map<int, TH1F *> m_cFlowHistos, m_cFlowHistosWeighted;
  	int m_N_xAOD, m_N_DxAOD;
  	double m_sumw_xAOD, m_sumw2_xAOD, m_sumw_DxAOD, m_sumw2_DxAOD;

  	// User-configurable selection options;
    double m_minMuonPT;          //! Minimum pT of muons for lepton veto (in GeV)
    double m_minElectronPT;      //! Minimum pT of electrons for lepton veto (in GeV)
    //float m_centraljet_eta_max;  //! Maximum eta of central jets
    //float m_centraljet_pt_min;   //! Minimum  pt of central jets (in GeV)
    //float m_forwardjet_eta_max;  //! Maximum eta of forward jets
    //float m_forwardjet_eta_min;  //! Minimum eta of forward jets
    //float m_forwardjet_pt_min;   //! Maximum pt of foeward jets (in GeV)
    //float m_yyb_min;             //! Minimum yyb mass to pass cut (in GeV)
    bool m_pt_priority_bjet;      //! Prioritize highest pt for btagged jet (passing WP) as opposed to first using "tightest btag WP". Use an integer





  	// Containers
  	xAOD::PhotonContainer m_allPhotons; //!
  	xAOD::PhotonContainer m_preSelPhotons; //!
  	xAOD::PhotonContainer m_selPhotons; //!
  	xAOD::PhotonContainer corrphotons; //!

  	xAOD::JetContainer m_allJets; //!
  	xAOD::JetContainer m_selJets; //!
  	xAOD::JetContainer m_jvtJets; //!

  	xAOD::ElectronContainer m_allElectrons; //!
  	xAOD::ElectronContainer m_selElectrons; //!

  	xAOD::MuonContainer m_allMuons; //!
  	xAOD::MuonContainer m_selMuons; //!



    private:

    /// \brief create a new cut-flow histogram
  	TH1F *makeCutFlowHisto(int id, TString suffix = "");

    void addBookKeeping(TH1F *cutflow, double N_xAOD, double N_DxAOD,
                      double sumw2_xAOD = -1, double sumw2_DxAOD = -1)
    {
    	int bin_xAOD = cutflow->FindBin(NxAOD), bin_DxAOD = cutflow->FindBin(NDxAOD);
    	cutflow->AddBinContent(bin_xAOD, N_xAOD);
    	cutflow->AddBinContent(bin_DxAOD, N_DxAOD);

    	if (sumw2_xAOD > 0.0)
    	{
      	cutflow->SetBinError(bin_xAOD, sqrt(pow(cutflow->GetBinError(bin_xAOD), 2)  + sumw2_xAOD));
      	cutflow->SetBinError(bin_DxAOD, sqrt(pow(cutflow->GetBinError(bin_DxAOD), 2) + sumw2_DxAOD));
    	}
  	}

  	/// brief get the "sample ID", meaning run number for data and MC channel number for MC
  	inline int getSampleID() { return HG::isMC() ? eventInfo()->mcChannelNumber() : eventInfo()->runNumber(); }

  	TH1F *getCutFlowHisto(bool withDalitz = true)
  	{
    	int ID = getSampleID() * (withDalitz ? -1 : 1);

    	if (TH1F *h = m_cFlowHistos[ID]) { return h; }

    	m_cFlowHistos[ID] = makeCutFlowHisto(ID, withDalitz ? "" : "_noDalitz");
    	return m_cFlowHistos[ID];
  	}

  	TH1F *getCutFlowWeightedHisto(bool withDalitz = true)
  	{
    	int ID = getSampleID() * (withDalitz ? -1 : 1);

    	if (TH1F *h = m_cFlowHistosWeighted[ID]) { return h; }

    	m_cFlowHistosWeighted[ID] = makeCutFlowHisto(ID, withDalitz ? "_weighted" : "_noDalitz_weighted");
    	return m_cFlowHistosWeighted[ID];
  	}

  	/// \brief fill the cut flow histograms
  	void fillCutFlow(CutEnum cut, double w);

  	/// \brief print a given cut flow histogram
  	void printCutFlowHisto(TH1F *h, int Ndecimals = 0);

  	/// \brief print all cut flow histograms stored in the maps
  	void printCutFlowHistos();

  	// apply cut flow
  	// returns enum corresponding to failed cut - or PASSALL if all cuts passed
  	CutEnum cutflow();

  	/*!
       \fn double weightBTagging(const xAOD::Jet *jet);

       \brief Fetch flavour tagging SF
     */
    double weightBTagging(const xAOD::Jet *jet);
    double weightJVT(xAOD::JetContainer &jets_noJVT);
    void doTruthMatch(xAOD::JetContainer &jets_sel);
    double dr(double phi1, double eta1, double phi2, double eta2);

    /*!
       \fn double weightJVT(HGamVLQTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param jets_noJVT The jet container for jets before JVT is applied.
     */


    /// Declares list of output variables to be written.
  /// configKey defines the key that specifies the list of variable names
  /// the name in the output file will be of the form outName+"Aux."+VARNAME
  void declareOutputVariables(TString outName, TString configKey, HG::StrV extra = {}, HG::StrV ignore = {});

  void decorateJet(xAOD::JetContainer &jets);





  	EL::StatusCode doReco(bool isSys = false);
  	EL::StatusCode doTruth();



protected:
    inline virtual HG::HcgamgamTool        *hcgamgamTool()        { return m_hcgamgamTool; }



public:
  // this is a standard constructor
  HcgamAnalysis() { }
  HcgamAnalysis(const char *name);
  virtual ~HcgamAnalysis();


  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();
  virtual EL::StatusCode fileExecute();

  // Functions for saving information
  void writeNominalOnly();
  void writeDetailed();

  // Functions for writting variables
  void writeNominalOnlyVars(bool truth = false);
  void writeDetailedVars(bool truth = false);



  // this is needed to distribute the algorithm to the workers
  ClassDef(HcgamAnalysis, 1);
};

#endif // hcgamgamanalysis_HcgamAnalysis_H
