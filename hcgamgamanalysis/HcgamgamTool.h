#pragma once

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"

// EDM include(s):
#include <AsgTools/AsgMessaging.h>


// ROOT include(s):
#include "TLorentzVector.h"
#include "TString.h"



typedef std::vector<int> vint;
typedef std::vector<float> vfloat;


namespace HG {

  class HcgamgamTool : public asg::AsgMessaging {
  public:

	HcgamgamTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler);

    virtual ~HcgamgamTool();

    virtual EL::StatusCode initialize(Config &config);


    enum HiggsHCCutflowEnum {HCPHOTONS = 0, HCATLEAST1JET = 1, HCJETPTETA =2, HCLEADJETPTETA = 3, DRJETGAM = 4, HCCTAG = 5, HCPASS = 6};
    enum m_yy_cat {SIDEBANDS = 0, VR = 1, SR = 2};


    void saveHCInfo(xAOD::PhotonContainer   photons,
                    xAOD::JetContainer      &jets,
                    xAOD::JetContainer      &jets_noJVT,
					xAOD::MuonContainer     &muons,
                    xAOD::ElectronContainer &electrons);

	void saveHCTruthInfo(const xAOD::TruthParticleContainer &photons,
                                  const xAOD::JetContainer   &jets,
                                  const xAOD::JetContainer   &bjets,
                                  const xAOD::JetContainer   &cjets,
                                  xAOD::TruthParticleContainer &muons,
                                  xAOD::TruthParticleContainer &electrons);


	private:

	HG::EventHandler *m_eventHandler; //!
	virtual HG::EventHandler *eventHandler();
	//Truthhandler if we need it later
	HG::TruthHandler *m_truthHandler; //!
	virtual HG::TruthHandler *truthHandler();


	// User-configurable selection options;
    double m_minMuonPT;          //! Minimum pT of muons for lepton veto (in GeV)
    double m_minElectronPT;      //! Minimum pT of electrons for lepton veto (in GeV)
    float m_jet_eta_max;  //! Maximum eta of central jets
    float m_truthjet_rapidity_max;  //! Maximum eta of truth jets
    float m_jet_pt;   //! Minimum  pt of central jets (in GeV)
    int m_Njets; //!
	float m_dRcut; //!



    unsigned long int m_evt;
    int m_eventCounter, m_debug;
    TString m_tagger;




    //void saveMapsToEventInfo();
    void saveMapsToEventInfo();
    void saveMapsToTruthEventInfo();



    void performSelection(xAOD::PhotonContainer    photons,
    					  xAOD::JetContainer       &jets,
    					  xAOD::JetContainer       &jets_noJVT,
                          xAOD::MuonContainer      &muons,
                          xAOD::ElectronContainer  &electrons);

    void performTruthSelection(const xAOD::TruthParticleContainer &photons,
                               const xAOD::JetContainer   &jets,
                               const xAOD::JetContainer   &bjets,
                               const xAOD::JetContainer   &cjets,
                               xAOD::TruthParticleContainer &muons,
                               xAOD::TruthParticleContainer &electrons);



	void performleadjetCutflow(TLorentzVector ph1,
							   TLorentzVector ph2,
							   xAOD::JetContainer &jets,
							   std::vector<int> centraljets);

	void performjetCutflow(TLorentzVector ph1,
						   TLorentzVector ph2,
						   xAOD::JetContainer &jets,
						   std::vector<int> centraljets);


	bool isCTagLoose(const xAOD::Jet &jet);
	bool isCTagTight(const xAOD::Jet &jet);
	bool isGoodJet(const xAOD::Jet *jet);
	void ClearTrue();
	void Clear();



	 /*!
       \fn double weightBTagging(const xAOD::Jet *jet);

       \brief Fetch flavour tagging SF
     */
    double weightCTagging(const xAOD::Jet *jet);

    /*!
       \fn double weightJVT(HGamVLQTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param jets_noJVT The jet container for jets before JVT is applied.
     */
    double weightJVT(xAOD::JetContainer &jets_noJVT);
    double discriminant(double pc, double pb, double pu, float fraction);


    std::map<TString, float>  m_eventInfoFloats;
    std::map<TString, int>    m_eventInfoInts;
    std::map<TString, char>   m_eventInfoChars;
    std::map<TString, vfloat> m_eventInfoVFloats;
    std::map<TString, vint>   m_eventInfoVInts;
    std::map<TString, int>    m_eventInfoTrueInts;


	int getChannelNumber() { return m_eventHandler->mcChannelNumber(); };
    bool m_writeTruthInfo;

    const xAOD::TruthParticle *ThisParticleFinal(const xAOD::TruthParticle *p);


    bool isGoodJ(const xAOD::Jet *);
    bool isCt(const xAOD::Jet &);
    bool isBt(const xAOD::Jet &);





  };

}
