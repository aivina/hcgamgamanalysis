#ifndef hcgamgamanalysis_hcskim_H
#define hcgamgamanalysis_hcskim_H

#include "HGamTools/SkimAndSlim.h"



class hcskim : public SkimAndSlim {
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  bool m_unfoldSkim; //!



  // helper functions defined by the user
private:
  EL::StatusCode skipXSecEvent(bool &skip);



  // public functions needed by EventLoop
public:
  // this is a standard constructor
  hcskim() { }
  hcskim(const char *name);
  virtual ~hcskim() { }

  // these are the functions inherited from SkimAndSlim
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();



  // this is needed to distribute the algorithm to the workers
  ClassDef(hcskim, 1);
};

#endif // HGamTools_hcskim_H
