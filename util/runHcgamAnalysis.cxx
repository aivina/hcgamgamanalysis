#include "hcgamgamanalysis/HcgamAnalysis.h"
#include "HGamAnalysisFramework/RunUtils.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  HcgamAnalysis *alg = new HcgamAnalysis("HcgamAnalysis");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
