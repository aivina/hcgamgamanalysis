#include <xAODRootAccess/Init.h>

// Local include(s):
#include "HGamAnalysisFramework/RunUtils.h"
#include "hcgamgamanalysis/hcskim.h"

#include <xAODRootAccess/Init.h>

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  hcskim *alg = new hcskim("hcskim");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
